<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);
Route::get('/logout', 'Auth\LoginController@logout');
// Route::post('/login', function(){
//   print_r('entered');exit;
// });

Route::get('/signup','Auth\RegisterController@index');
Route::post('/signup','Auth\RegisterController@create');

Route::get('/',"ProjectController@project_view");
Route::get('/index',"ProjectController@project_view");

Route::post('/login',"Auth\LoginController@loginUser");

Route::get('/create_project','ProjectController@createProjectView');
Route::post('/create_project','ProjectController@createProject');

Route::get('/project_list' , 'ProjectController@project_list');
Route::post('/project_all' , 'ProjectController@project_all');
// Route::get('/project_file/{id}','ProjectController@project_file');
Route::get('/project/edit/','ProjectController@project_edit');
Route::post('/project/update/','ProjectController@project_update');
Route::get('/project/view/','ProjectController@project_view');

Route::get('/project/setting/','ProjectController@project_setting');
Route::get('/project/history/clear','ProjectController@project_hisory');
Route::post('/client/delete/','ProjectController@client_delete');
Route::post('/client/update/','ProjectController@client_update');

Route::post('/company/create/','ProjectController@company_create');
Route::post('/company/delete/','ProjectController@company_delete');
Route::post('/company/update/','ProjectController@company_update');

Route::post('/change_p_status','ProjectController@change_p_status');

Route::post('/delete_record','ProjectController@delete_record');


Route::get('/ticket/new','ProjectController@new_ticketView');
Route::post('/new_ticket','ProjectController@new_ticketCreate');

Route::get('/ticket/list','ProjectController@ticket_list');
Route::get('/ticketview/{id}','ProjectController@ticket_view');
Route::get('/ticket/{id}/edit','ProjectController@ticket_edit');
Route::post('/ticket_edit','ProjectController@ticket_edit');
Route::get('/ticket/{id}/delete','ProjectController@ticket_delete');
Route::post('/ticket_update','ProjectController@ticket_update');
Route::get('/ticket/{id}/print','ProjectController@ticket_print');

Route::get('/ticket/{id}/submit','ProjectController@ticket_submit');
Route::get('/ticket/{id}/history/clear','ProjectController@ticket_history_clear');
Route::get('/ticket/{id}/revise','ProjectController@ticket_revise');
Route::get('/ticket/{id}/approve','ProjectController@ticket_approve');
Route::get('/ticket/{id}/close','ProjectController@ticket_close');


Route::get('/ticket/setting','OtherController@ticket_setting');

Route::post('/labor/delete','OtherController@labor_delete');
Route::post('/labor/create','OtherController@labor_create');
Route::post('/labor/update','OtherController@labor_update');

Route::post('/rate/delete','OtherController@rate_delete');
Route::post('/rate/create','OtherController@rate_create');
Route::post('/rate/update','OtherController@rate_update');

Route::post('/create_client','ProjectController@create_client');

Route::get('/setting','ProjectController@setting');
Route::post('/setting/update','ProjectController@storesetting');
Route::post('/setting/logo','ProjectController@update_logo');

Route::resource('/order','OrdercController');

Route::get('/order/{id}/view','OrdercController@order_view');
Route::get('/order/{id}/delete','OrdercController@order_delete');
Route::post('/order/{id}/update','OrdercController@order_update');
Route::get('/order/{id}/edit','OrdercController@store');
Route::get('/order/{id}/submit','OrdercController@order_submit');
Route::get('/order/{id}/revise','OrdercController@order_revise');
Route::get('/order/{id}/approve','OrdercController@order_approve');
Route::get('/order/{id}/close','OrdercController@order_close');
Route::get('/order/{id}/print','OrdercController@print');
Route::post('/order/{id}/tickets','OrdercController@tickets');

Route::get('/order/{id}/oapprove','OrdercController@order_oapprove');
Route::get('/order/{id}/complete','OrdercController@order_complete');
Route::get('/order/{id}/history/clear','OrdercController@history_clear');
Route::post('/order/payment','OrdercController@payment_update');

Route::resource('timesheet','TimesheetController');
Route::post('/timesheet/destroy','TimesheetController@destroy');
Route::post('/timesheet/update','TimesheetController@update');
Route::get('/timesheet/setting','TimesheetController@timesheet_setting');

Route::post('/worker/create','TimesheetController@worker_create');
Route::post('/worker/delete','TimesheetController@worker_delete');
Route::post('/worker/update','TimesheetController@worker_update');
Route::resource('/account','ProfileController');
Route::post('/password/update','ProfileController@password_update');









