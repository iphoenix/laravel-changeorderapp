<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin:: Aside -->
    <div class="kt-aside__brand kt-grid__item  " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <a href="{{url('index')}}">
                <img alt="Logo" id='top_logo' src="{{asset('storage/app/public/photos/'.$setting->logo_url)}}" />
            </a>
        </div>
    </div>

    <!-- end:: Aside -->

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu  kt-aside-menu--dropdown " data-ktmenu-vertical="1" data-ktmenu-dropdown="1" data-ktmenu-scroll="0">
            <ul class="kt-menu__nav ">

                <li class="kt-menu__item   kt-menu__item--submenu @if(isset($active) && $active == 1) kt-menu__item--active @endif " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-layers-1"></i><span class="kt-menu__link-text">Projects</span></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Projects</span></span></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('project/view')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Project view</span></a></li> 
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/project/edit')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Edit Project</span></a></li>
                           
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/project/setting')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Project Setting</span></a></li>

                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu  @if(isset($active) && $active == 2) kt-menu__item--active @endif " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fa fa-ticket-alt"></i><span class="kt-menu__link-text">Ticket</span></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Ticket</span></span></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('ticket/new')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">New Ticket</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('ticket/list')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Ticket List</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/ticket/setting')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Ticket Setting</span></a></li>
                        </ul>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu  @if(isset($active) && $active == 3) kt-menu__item--active @endif " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fab fa-opencart"></i><span class="kt-menu__link-text">Order</span></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Order</span></span></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/order/create')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">New Order</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/order')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Order List</span></a></li>
                        </ul>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu  @if(isset($active) && $active == 4) kt-menu__item--active @endif " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fa fa-calendar-alt"></i><span class="kt-menu__link-text">Timesheet</span></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Timesheet</span></span></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/timesheet/create')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">New Timesheet</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/timesheet')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Timesheet List</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/timesheet/setting')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Timesheet Setting</span></a></li>
                        </ul>
                    </div>
                </li>


                <li class="kt-menu__item  @if(isset($active) && $active == 5) kt-menu__item--active @endif " aria-haspopup="true"><a href="{{url('setting')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-settings"></i><span class="kt-menu__link-text">Setting</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{url('/logout')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-logout"></i><span class="kt-menu__link-text">Logs</span></a></li>
            </ul>
        </div>
    </div>

    <!-- end:: Aside Menu -->
</div>

<!-- end:: Aside -->
