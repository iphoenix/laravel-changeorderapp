<!--begin::Base Path (base relative path for assets of this page) -->
<base href="../">

<!--end::Base Path -->
<meta charset="utf-8" />
<title>Order management system</title>
<meta name="description" content="Latest updates and statistic charts">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!--begin::Fonts -->
{{-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script> --}}
<script>
    // WebFont.load({
    //     google: {
    //         "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
    //     },
    //     active: function() {
    //         sessionStorage.fonts = true;
    //     }
    // });
</script>

<!--end::Fonts -->

<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{asset('public/assets/metronic/custom/fullcalendar/fullcalendar.bundle.css')}} rel="stylesheet" type="text/css" />

<!--end::Page Vendors Styles -->

<!--end:: Global Mandatory Vendors -->

<!-- <link href="{{asset('public/assets/metronic/vendors/general/tether/dist/css/tether.css')}}" rel="stylesheet" type="text/css" /> -->
<link href="{{asset('public/assets/metronic/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('public/assets/metronic/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/general/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('public/assets/metronic/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/custom/vendors/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/custom/vendors/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/custom/vendors/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/assets/metronic/vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/custom-css/loading.css')}}" rel="stylesheet" type="text/css" />
<style>
    @media (max-width: 1024px) {
        #mobile_logo {
            width:30px;
        }
    }

    @media (min-width: 1025px) {
        #top_logo {
            width:55px;
        }
    }
</style>
