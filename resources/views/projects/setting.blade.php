@extends('layout.master')

<style>
    .attachment{
        cursor: pointer;
    }
    #new_client{
        top:30% !important;
    }
    #new_company{
        top:15% !important;
    }
    #company_update_form{
        top:15% !important;
    }
    #client_update_form{
        top:15% !important;
    }
</style>
@section('custom-css')

@endsection
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Projects </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Project List</span>
    {{-- <a href="{{url('/create_project')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New Project
        </a> --}}
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            {{-- <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span> --}}
            {{-- <h3 class="kt-portlet__head-title">
                HTML Table
                <small>Datatable initialized from HTML table</small>
            </h3> --}}
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/create_project')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Project
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">

                </div>
                <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                    <a href="#" class="btn btn-default kt-hidden">
                        <i class="la la-cart-plus"></i> New Order
                    </a>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--end: Search Form -->
    </div>

    <div class="kt-portlet__body kt-portlet__body--fit">
        <div class="row">
            <div class="col-md-5">
                <div class="kt-portlet">
                    <div class="kt-portlet__head row">
                        <div class="col-md-6 text-left mt-3">
                            <h2><i class="fa fa-user-tie"></i>&nbsp;&nbsp;&nbsp;Clients</h2>
                        </div>
                        <div class="col-md-6 text-right mt-3">
                            <button type="button" data-toggle='modal' data-target="#new_client" id="add_client" class="btn btn-primary btn-sm btn-elevate  btn-icon"><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                        <div class="kt-section">

                            <div class="kt-section__content">
                                <table class="table" id="client" width="100%">
                                    <thead>
                                        <tr>
                                            <th title="Field #1">Client name</th>
                                            <th title="Field #2">Created time</th>
                                            <th title="Field #3" style="width:30px !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($clients))
                                            @foreach($clients as $client)
                                                <tr>
                                                    <td>{{$client->client_name}}</td>
                                                    <td>{{$client->created_at}}</td>
                                                    <td>
                                                        <a onclick="delete_client({{$client->id}})" id="{{'delete_client'.$client->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
                                                        <i class="fa fa-trash-alt"></i>
                                                        </a>
                                                        <a onclick="update_client({{$client->id}})"  class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" >
                                                            <i class="fa fa-edit"></i>
                                                            </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                </div>
            </div>
            <div class="col-md-7">
                <div class="kt-portlet">
                    <div class="kt-portlet__head row">
                        <div class="col-md-6 text-left mt-3">
                            <h2><i class="fa fa-building"></i>&nbsp;&nbsp;&nbsp;Companies</h2>
                        </div>
                        <div class="col-md-6 text-right mt-3">
                            <button type="button" id="add_company" data-toggle='modal' data-target="#new_company" class="btn btn-primary btn-sm btn-elevate  btn-icon"><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                        <div class="kt-section">

                            <div class="kt-section__content">
                                <table class="table" id="company" width="100%">
                                    <thead>
                                        <tr>

                                            <th title="Field #1">Company name</th>
                                            <th title="Field #2">Address</th>
                                            <th title="Field #3">Phone</th>
                                            <th title="Field #4">EIN</th>
                                            <th title="Field #5">Created time</th>
                                            <th title="Field #6" style="width:30px !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($companies))
                                            @foreach($companies as $company)
                                                <tr>
                                                <td>{{$company->name}}</td>
                                                <td>{{$company->address}}</td>
                                                <td>{{$company->phone}}</td>
                                                <td>{{$company->ein}}</td>
                                                <td>{{$company->created_at}}</td>
                                                <td>
                                                    <a onclick="delete_company({{$company->id}})" id="{{'delete_company'.$company->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md " title="Delete">
                                                    <i class="fa fa-trash-alt"></i>
                                                    </a>

                                                    <a onclick="update_company({{$company->id}})"class="btn btn-sm btn-clean btn-icon btn-icon-md " >
                                                        <i class="fa fa-edit"></i>
                                                        </a>

                                                </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Client Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new client</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!--begin::new_company Modal-->
<div class="modal fade" id="new_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Create New Company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">Company Name:</label>
                        <input type="text" name='name' class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">Address:</label>
                        <input type="text" name='address' class="form-control"  required>
                    </div>
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">Phone:</label>
                        <input type="text" name='phone' class="form-control"   required>
                    </div>
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">EIN:</label>
                        <input type="text" name='ein' class="form-control" required >
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new_company_btn" class="btn btn-primary">Create new company</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::new_company Modal-->
<div class="modal fade" id="company_update_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Update  Company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <input type="hidden" name="com_id" id="com_id">
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">Company Name:</label>
                        <input type="text" name='name' id="com_name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">Address:</label>
                        <input type="text" name='address' id="com_address" class="form-control"  required>
                    </div>
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">Phone:</label>
                        <input type="text" name='phone' id="com_phone" class="form-control"   required>
                    </div>
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label">EIN:</label>
                        <input type="text" name='ein' id="com_ein" class="form-control" required >
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="company_update_btn" class="btn btn-primary">Update company</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end::Modal-->



<!--begin::update client Modal-->
<div class="modal fade" id="client_update_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Update client name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <input type="hidden" id="client_id" name="client_id" value="">
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label"> Name:</label>
                        <input type="text" name='name' id="client_name" class="form-control text-right" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="client_update_btn" class="btn btn-primary">Update client name</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end:update client Modal-->


<!-- end:: Content -->
@endsection

@section('page-js')


    <script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/datatables.bootstrap4.min.js')}}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $("#client").DataTable({
                aaSorting: [],
                responsive: true,

                columnDefs: [
                {
                    responsivePriority: 1,
                    targets: 0
                },
                {
                    responsivePriority: 2,
                    targets: -1
                }
                ]
            });

            $("#company").DataTable({
                aaSorting: [],
                responsive: true,

                columnDefs: [
                {
                    responsivePriority: 1,
                    targets: 0
                },
                {
                    responsivePriority: 2,
                    targets: -1
                }
                ]
            });

            $(".dataTables_filter input")
                .attr("placeholder", "Search here...")
                .css({
                width: "100px",
                display: "inline-block"
                });

            $('[data-toggle="tooltip"]').tooltip();
        });

        "use strict";
    // Class definition

    var KTDatatableHtmlTableDemo = function() {


        var new_client = function()
                {
                    $('#new-client-btn').click(function(){
                        var _this = $(this);
                        var client_modal = $('#new_client');
                        var form = _this.closest('form');
                        var inputs = $('#new_client input');
                        form.validate({
                            rules:{
                                new_client_name:{
                                    required:true,
                                },
                            }
                        })

                        if(!form.valid()){
                            return;
                        }
                        _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                        form.ajaxSubmit({
                        url: "{{url('/create_client')}}",
                        success: function(response, status, xhr, $form) {
                            // similate 2s delay
                            if(response.success == 'true'){
                                swal.fire("New client has been created!", "Please use new client.","success");
                                _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                client_modal.modal('hide');
                                form[0].reset();
                                setTimeout(function(){ location.reload(true); }, 1000);



                                //location.reload();
                            } else {
                                swal.fire("The client existed with same name!","Please use another name.", "error");
                                _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                client_modal.modal('hide');
                                form[0].reset();

                                // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                            }
                        },
                        error: function() {

                                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                        }
                    });


                    })

                }
        var new_company = function()
        {
            $('#new_company_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#new_company');
                var form = _this.closest('form');
                var inputs = $('#new_client input');
                form.validate({
                    rules:{
                        new_client_name:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/company/create')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success == 'true'){
                        swal.fire("New company has been created!", "Please use new company.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The company existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }

        var update_client = function()
        {
            $('#client_update_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#client_update_form');
                var form = _this.closest('form');
                var inputs = $('input');
                var btn = $('#client_update_btn');
                form.validate({
                    rules:{
                        name:{
                            required:true,
                        },
                        client_id:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/client/update')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success){
                        swal.fire("The client name has been updated!", "Please make sure it.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The client name existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }

        var update_company = function()
        {
            $('#company_update_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#company_update_form');
                var form = _this.closest('form');
                var inputs = $('input');
                var btn = $('#company_update_btn');
                form.validate({
                    rules:{
                        name:{
                            required:true,
                        },
                        address:{
                            required:true,
                        },
                        phone:{
                            required:true,
                        },
                        ein:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/company/update')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success){
                        swal.fire("The client name has been updated!", "Please make sure it.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The client name existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }

        var stop_enter = function (){
            $('form input').keydown(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();

                    swal.fire("Please click submit button!","- Do not use enter key-.", "error");
                    return false;
                }
            });
        }


        return {
            // Public functions
            init: function() {

                new_client();
                new_company();
                update_client();
                stop_enter();
                update_company();

            },
        };
    }();

    jQuery(document).ready(function() {
        KTDatatableHtmlTableDemo.init();
    });

    function delete_client(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/client/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'The client has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_client'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);
                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This client name is safe',
                'error'
                )
            }
        })

    }

    function delete_company(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/company/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'The company has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_company'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);

                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This company name is safe',
                'error'
                )
            }
        })

    }

    function edit_company(record_id){




        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/company/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_company'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);

                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your  file is safe',
                'error'
                )
            }
        })

    }


    function update_client(client_id){
        $('#client_id').val(client_id);
        var id = '#delete_client'+client_id;
        $('#client_name').val($(id).parent().prev().prev().text());
        $('#client_update_form').modal('show');
    }

    function update_company(com_id){
        $('#com_id').val(com_id);
        var id = '#delete_company'+com_id;
        $('#com_name').val($(id).closest('tr').find('td').eq(0).text());
        $('#com_address').val($(id).closest('tr').find('td').eq(1).text());
        $('#com_phone').val($(id).closest('tr').find('td').eq(2).text());
        $('#com_ein').val($(id).closest('tr').find('td').eq(3).text());
        //console.log($(id).closest('tr').find('td').eq(0).html());
        $('#company_update_form').modal('show');
    }
    </script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


