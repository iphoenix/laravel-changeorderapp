@extends('layout.master')

@section('custom-css')

<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />
<style>
    #p_status{
        cursor: pointer;
    }
</style>

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Projects </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc"> project view</span>
    {{-- <a href="{{url('/create_project')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New
        </a> --}}
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-8">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head row">
                    <div class="col-md-6 text-left mt-3">
                        <h2><i class="fa fa-eye"></i>&nbsp;&nbsp;&nbsp;Project View</h2>
                    </div>
                    <div class="col-md-6 text-right mt-3" id="p_status">
                        @if($project->p_status == 0)
                            <span  class="attachment mt-2 kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill ">Draft</span>
                        @elseif($project->p_status == 1)
                            <span  class="attachment mt-2 kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill ">Active</span>
                        @endif
                    </div>
                </div>

                <!--begin::Form-->
                <div class="kt-form kt-form--label-right">
                   
                    <div class=" row pt-5">
                        <div class=" col-md-6 ">
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Project name:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->project_name}}</h4></span>
                                </div>
                            </div>
                          
                        </div>
                        <div class='col-md-6'></div>
                        <div class=" col-md-6 ">
                            
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Client:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->getClient->client_name}}</h4></span>
                                </div>
                            </div>    
                            
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Company:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->company->name}}</h4></span>
                                </div>
                            </div>

                        </div>
                        <div class='col-md-6'>
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Project Number:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->project_num}}</h4></span>
                                </div>
                            </div>

                        </div>
                        <div class='col-md-6'>
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Start Date:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->project_startdate}}</h4></span>
                                </div>
                            </div>

                        </div>
                        <div class='col-md-6'>
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>manager:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->getManager->name}}</h4></span>
                                </div>
                            </div>

                        </div>
                        <div class='col-md-6'>
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Advenced Form:</h4></label>
                                <div class="col-6 col-form-label">
                                    <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
                                        <label>
                                            <input type="checkbox" id="ticket_op" name="" @if($project->ticket_option == 1) checked ='checked' @endif disabled>
                                            <span></span>
                                        </label>
                                        <input type="hidden" name="ticket_option" id="ticket_option"  value="{{$project->ticket_option}}">
                                    </span>                               
                                </div>
                            </div>
                           
                            <div class="col-3">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Files:</h4></label>
                                <div class="col-8">
                                    @php
                                        //var_dump(json_decode($ticket->original_photoname));
                                        $filename_array = $project->filename;
                                        if(!empty($project->original_filename)){
                                            //var_dump(json_decode($ticket->photoname));
                                            $index = 1;
                                            foreach($project->original_filename as $key=> $file){
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='attached-file  kt-badge mt-3 kt-badge--success kt-badge--inline kt-badge--pill ' data-container='body' data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='".$file."'"." onClick=fileDownload('".asset("storage/app/public/project/".$filename_array[$key])."')". ">".$index."</span>";
                                            $index++;
                                            }
                                        }
                                        @endphp
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'></div>
                        <div class="col-md-6">
                            <div class="form-group form-group-xs row">
                                <label class="col-4 col-form-label"><h4>Address:</h4></label>
                                <div class="col-8">
                                    <span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$project->address}}</h4></span>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group form-group-xs row">
                                <div class="col-4 col-form-label text-right"><h4>Crews Name:</h4></div>
                                <div class="col-4 col-form-label text-right"><h4>Crews ID:</h4></div>
                             
                            </div>
                                @if(isset($project->crew_id) && !empty($project->crew_id))
                                    @php
                                        $crew_id = $project->crew_id;
                                        $crew_name= $project->crew_name;
                                        foreach($crew_id as $key =>$crew){
                                            echo  "<div class='form-group form-group-xs row'><div class='col-4  text-right'><h5>{$crew_name[$key]}</h5></div><div class='col-4 text-right'><h5>{$crew}</h5></div></div>";
                                        }
                                    @endphp
                                @endif
                            </div>
                    </div>
                </div>
                <!--end::Form-->
                <div class="kt-portlet__foot mt-3">
                        <div class="kt-form__actions">
                            <div class="row">
                               
                            </div>
                        </div>
                    </div>
            </div>
            <!--end::Portlet-->
        </div>
        <div class="col-lg-4">
            <!--Begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title" style="font-size:2rem;">
                            history
                        </h2>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                        <a href="{{url('/project/history/clear')}}" class="btn btn-clean btn-sm btn-bold" >
                            <i class="fa fa-trash-alt"></i> History clear
                        </a>
                    </div>

                </div>
                <div class="kt-portlet__body">


                    <!--begin::Preview-->
                        <div class="kt-demo">
                            <div class="kt-demo__preview">
                                <div class="kt-list-timeline">
                                    <div class="kt-list-timeline__items">
                                        @if (!empty($project_log))
                                            @foreach($project_log as $his)
                                                @if($his->status == 0)
                                                    <div class="kt-list-timeline__item">
                                                        <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                        <span class="kt-list-timeline__icon flaticon2-position kt-font-danger"></span>
                                                        <span class="kt-list-timeline__text">manager: {{$his->user->name}}
                                                            <span class="kt-badge kt-badge--success kt-badge--inline">Item created</span></span>
                                                        <span class="kt-list-timeline__time" style="width:130px;">{{date('F j, Y, g:i a', strtotime($his->created_at))}}</span>
                                                    </div>
                                                @elseif($his->status == 1)
                                                    <div class="kt-list-timeline__item">
                                                        <span class="kt-list-timeline__badge kt-list-timeline__badge--primary"></span>
                                                        <span class="kt-list-timeline__icon flaticon2-note kt-font-primary"></span>
                                                        <span class="kt-list-timeline__text">manager: {{$his->user->name}} <span class="kt-badge kt-badge--info kt-badge--inline">Item updated</span></span>
                                                        <span class="kt-list-timeline__time" style="width:130px;">{{date('F j, Y, g:i a', strtotime($his->created_at))}}</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!--end::Preview-->
                </div>
            </div>

            <!--End:: Portlet-->
        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Client Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name">
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new client</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')


    <!--begin::Page Vendors(used by this page) -->

    <script>
        "user strict";
        var create_project = function(){

            var create_p= $('#create_project');
            var submit_new_project = function(){
                $('#submit-project').click(function(e){
                    console.log('sdfsd');
                    e.preventDefault();
                    var btn = $(this);
                    var form = btn.closest('form');
                    form.validate({
                        rules: {
                            project_name: {
                                required:true,
                            },
                            client_id:{
                                required:true,
                            },
                            project_num:{
                                number: true,
                                required:true,
                            },
                            project_startdate:{
                                required: true,
                            },
                            user_id:{
                                required: true,
                            },
                            manager_id:{
                                required: true,
                            },
                            address:{
                                required: true,
                            },
                            fileupload: {
                                required: true,
                            },


                        }
                    })

                    if (!form.valid()) {
                        return;
                    }

                    form.submit();


                });
            }

            var new_client = function()
            {
                $('#new-client-btn').click(function(){
                    var _this = $(this);
                    var client_modal = $('#new_client');
                    var form = _this.closest('form');
                    var inputs = $('#new_client input');
                    form.validate({
                        rules:{
                            new_client_name:{
                                required:true,
                            },
                        }
                    })

                    if(!form.valid()){
                        return;
                    }
                    _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                    form.ajaxSubmit({
                    url: "{{url('create_client')}}",
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        if(response.success == 'true'){
                            swal.fire("New client has been created!", "Please use new client.","success");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();


                            //location.reload();
                        } else {
                            swal.fire("The client existed with same name!","Please use another name.", "error");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();

                            // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                        }
                    },
                    error: function() {

                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                    }
                });


                })

            }

            var file_change = function(){
                $('.fileupload-group input').change(function () {
                    $('.fileupload-group p').text(this.files.length + " file(s) selected");
                });
            }

            var ticket_option = function(){
                $('#ticket_op').click(function(){
                    _this = $('#ticket_option').val();
                    if(_this == 1){
                        $('#ticket_option').val(0);
                    }   else {
                        $('#ticket_option').val(1);
                    }

                });
            }




            //public function
            return {
                init: function(){
                    ticket_option();
                    submit_new_project();
                    new_client();
                    file_change();
                  }
            };


        }();

        $(document).on('click','#add_crew', function(e){
            e.preventDefault();
            var content = "<div class='row pb-1'><div class='col-md-6'><input type='text' class='form-control' name='crew_name[]' placeholder='Enter crew name ' required></div><div class='col-md-5'><input type='text' class='form-control' name='crew_id[]' placeholder='Enter crew ID' required></div><div class='col-md-1'><button class='btn btn-danger' class='remove_crew'  type='button'><i class='flaticon-delete-1 remove_crew'></i></button></div></div>"

            $("#crew_group").append(content);

        }).on('click','.remove_crew', function(){

                $(this).closest('.row').remove();
        })

        jQuery(document).ready(function(){
            create_project.init();
        });
        $('#kt_select2_10').select2({
            placeholder: "Select an manager",
            minimumResultsForSearch: Infinity
        });

        $('#client_id').select2({
            placeholder: "Select an client",
            minimumResultsForSearch: Infinity
        });

        $('#company_id').select2({
            placeholder: "Select an company",
            minimumResultsForSearch: Infinity
        });

        function fileDownload(url){
        window.URL.revokeObjectURL(url);

        swal.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: false,

            // confirmButtonText: 'Yes, delete it!'
            title: 'Do you want to download file?',
            html: "<a  href='"+url+"' target='_blank' >Preview file.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageAlt: 'Custom image',
            animation: true
        });
    } 


    </script>


    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif

    @if($errors->any())
        <script>
            swal.fire("{{ implode('', $errors->all(':message')) }}", "Please confirm it.","error");
        </script>
    {{ implode('', $errors->all('<div>:message</div>')) }}
    @endif




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


