@extends('layout.master')

<style>
    .attachment{
        cursor: pointer;
    }
</style>
@section('custom-css')

@endsection
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Projects </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Project List</span>
    {{-- <a href="{{url('/create_project')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New Project
        </a> --}}
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            {{-- <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span> --}}
            {{-- <h3 class="kt-portlet__head-title">
                HTML Table
                <small>Datatable initialized from HTML table</small>
            </h3> --}}
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/create_project')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Project
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">

                </div>
                <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                    <a href="#" class="btn btn-default kt-hidden">
                        <i class="la la-cart-plus"></i> New Order
                    </a>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--end: Search Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">

        <!--begin: Datatable -->

        <table class="table" id="example" width="100%">
            <thead>
                <tr>
                    <th title="Field #1">Created Date</th>
                    <th title="Field #2">Project Name</th>
                    <th title="Field #3">Project Number</th>
                    <th title="Field #4">Client</th>
                    <th title="Field #5">Company</th>
                    <th title="Field #6">Attached file</th>
                    <th title="Field #7">Status</th>
                    <th title="Field #8">Manager</th>
                    <th title="Field #9">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($project_list as $row)
                    <tr>
                        <td>{{date('m/d/Y',strtotime($row->created_at))}}</td>
                        <td>{{$row->project_name}}</td>
                        <td>{{$row->project_num}}</td>
                        <td>{{$row->getClient->client_name}}</td>

                        <td>{{$row->Company->name}}</td>
                        <td>
                            @php
                                //var_dump(json_decode($ticket->original_photoname));
                                $filename_array = $row->filename;
                                if(!empty($row->original_filename)){
                                    //var_dump(json_decode($ticket->photoname));
                                    $index = 1;
                                    foreach($row->original_filename as $key=> $file){
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='attachment kt-badge  kt-badge--success kt-badge--inline kt-badge--pill ' data-container='body' data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='".$file."'"." onClick=fileDownload('".asset("storage/app/public/project/".$filename_array[$key])."')". ">".$index."</span>";
                                    $index++;
                                    }
                                }
                            @endphp
                            {{-- <a href="{{url('project_file')."/".$row->id}}" >{{$row->original_filename}}<a> --}}

                        </td>
                        <td>
                            @if($row->p_status == 0)
                                <span  class="attachment kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill ">Draft</span>
                            @elseif($row->p_status == 1)
                                <span  class="attachment kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill ">Active</span>
                            @endif
                        </td>
                        <td>{{$row->getmanager->name}}</td>
                        <td>
                            <a onclick="deleted_record({{$row->id}})" id="{{'del'.$row->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
							    <i class="fa fa-trash-alt"></i>
                            </a>
                            <a href="{{url('/project/edit/'.$row->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record">
							    <i class="fa fa-edit"></i>
                            </a>
                            <a onclick="active({{$row->id}})" id="{{'active'.$row->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record">
							    <i class="fa fa-external-link-square-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

            </tbody>
            <tfoot>
                <tr>
                    <th title="Field #1">Created Time</th>
                    <th title="Field #2">Project Name</th>
                    <th title="Field #3">Project Number</th>
                    <th title="Field #4">Client</th>
                    <th title="Field #5">Company</th>
                    <th title="Field #6">Attached file</th>
                    <th title="Field #7">Status</th>
                    <th title="Field #8">Manager</th>
                    <th title="Field #9">Action</th>
                </tr>
            </tfoot>
        </table>

        <!--end: Datatable -->
    </div>
</div>
<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')


    <script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/datatables.bootstrap4.min.js')}}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {

            $('#example').DataTable( {
                initComplete: function () {
                    this.api().columns([3, 4, 6,7]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">Show all</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            var val = $('<div/>').html(d).text();
                            select.append( '<option value="' + val + '">' + val + '</option>' );
                            // select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            } );

            $(".dataTables_filter input")
                .attr("placeholder", "Search here...")
                .css({
                width: "300px",
                display: "inline-block"
                });
            $('[data-toggle="tooltip"]').tooltip();
        });


// Class definition
    function deleted_record(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "If you delete this project, related orders and tickets will be deleted.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/delete_record') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        var del_id = '#del'+record_id;
                        $(del_id).closest('tr').remove();

                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )


                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your  file is safe',
                'error'
                )
            }
        })

    }


    function active(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to change this status!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, change it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}',
                }
            });
            jQuery.ajax({
                url: "{{ url('/change_p_status') }}",
                method: 'post',
                data: {
                    record_id: record_id,

                },
                success: function(result){
                    var del_id = '#del'+record_id;
                    var element =  $(del_id).closest('td').prev().prev().children();
                    if(result.status == 1){
                        console.log(1);
                        element.text('Active');
                        element.removeClass('kt-badge--danger').addClass('kt-badge--primary');

                    } else {
                        element.text('Draft');
                        element.removeClass('kt-badge--primary').addClass('kt-badge--danger');
                    }


                    swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'The status has been updated.',
                    'success'
                    )


                }
            });


        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'You have cancelled the modification of the status',
            'error'
            )
        }
    })

    }

    function fileDownload(url){
        window.URL.revokeObjectURL(url);

        swal.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: false,

            // confirmButtonText: 'Yes, delete it!'
            title: 'Do you want to download file?',
            html: "<a  href='"+url+"' target='_blank' >Preview file.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageAlt: 'Custom image',
            animation: true
        });

    }
    </script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


