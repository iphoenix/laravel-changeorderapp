@extends('layout.master')

@section('custom-css')

<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />
<style>
    #p_status{
        cursor: pointer;
    }
    .is-invalid{
    border:1px solid red !important;
    }
</style>

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Projects </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Edit project</span>
    {{-- <a href="{{url('/create_project')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New
        </a> --}}
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-8">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head row">
                    <div class="col-md-6 text-left mt-3">
                        <h2><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Edit</h2>
                    </div>
                    <div class="col-md-6 text-right mt-3" id="p_status">
                        @if($project->p_status == 0)
                            <span onclick="active({{$project->id}})" id="{{'active'.$project->id}}" class="attachment mt-2 kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill ">Draft</span>
                        @elseif($project->p_status == 1)
                            <span  onclick="active({{$project->id}})" id="{{'active'.$project->id}}" class="attachment mt-2 kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill ">Active</span>
                        @endif
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="create_project" action="{{url('/project/update')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="project_id" value="{{$project->id}}"/>
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}"/>
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group row divide">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="main-title">Project Name:</label>
                                <input type="text" class="form-control" name="project_name" placeholder="Enter project name" value="{{$project->project_name}}">
                                <span class="form-text text-muted">Please enter new project name</span>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="main-title">Client:</label>
                                   <div class="input-group">
                                        <select class="form-control kt-select2" id="client_id" name="client_id">
                                            <option></option>
                                            @foreach($clients as $client)

                                                <option  value="{{$client->id}}" @if($client->id == $project->client_id) selected @endif   >{{$client->client_name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" data-toggle='modal' data-target="#new_client" type="button">+ new </button>
                                        </div>
                                   </div>

                                    <span class="form-text text-muted">Please select client's name</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="main-title">Company:</label>
                                   <div class="input-group">
                                        <select class="form-control kt-select2" id="company_id" name="company_id">
                                            <option></option>
                                            @foreach($companies as $com)
                                                <option  value="{{$com->id}}" @if ($com->id == $project->company_id)
                                                    selected
                                                @endif>{{$com->name}}</option>
                                            @endforeach
                                        </select>
                                   </div>

                                    <span class="form-text text-muted">Please select client's name</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row divide">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="main-title">Project Number:</label>
                                <input type="text" class="form-control" name="project_num" placeholder="Enter project name" value="{{$project->project_num}}">
                                <span class="form-text text-muted">Please enter project number</span>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group row">
                                    <label class="main-title">Start Date:</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="project_startdate" readonly placeholder="Select date" id="kt_datepicker_2" value="{{$project->project_startdate}}" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <span class="form-text text-muted">Please enter start date</span>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row divide">
                            <div class="col-sm-12 col-md-6">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="main-title">Manager:</label>
                                            <div class="input-group">
                                                <select class="form-control kt-select2" id="kt_select2_10" name="manager_id">
                                                    <option></option>
                                                    @foreach($users as $user)
                                                        <option value="{{$user->id }}" @if($project->manager_id == $user->id) selected @endif  >{{$user->name}}</option>
                                                    @endforeach
												</select>
                                           </div>

                                            <span class="form-text text-muted">Please manager</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                        <label class="main-title">Advenced Foreman Ticket Form:</label><br>
                                        <label class="sub-title">includes Labor, Material, and Equipment breakdowns</label>
                                        <div class="col-3">
                                            <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
                                                <label>
                                                    <input type="checkbox" id="ticket_op" name="" @if($project->ticket_option == 1) checked ='checked' @endif >
                                                    <span></span>
                                                </label>
                                                <input type="hidden" name="ticket_option" id="ticket_option"  value="{{$project->ticket_option}}">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label class="main-title">Crews:</label>
                                    </div>
                                    <div class="col-md-6 text-right pb-1">

                                        <button class="btn btn-primary" id="add_crew"  type="button">+ add crew</button>

                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12" id="crew_group">
                                        @if(isset($project->crew_id) && !empty($project->crew_id))
                                            @php
                                                $crew_id = $project->crew_id;
                                                $crew_name= $project->crew_name;
                                                foreach($crew_id as $key =>$crew){
                                                    echo  "<div class='row pb-1'><div class='col-md-5'><input type='text' class='form-control' name='crew_name[]' value ='{$crew_name[$key]}' placeholder='Crew name'  required></div><div class='col-md-5'><input type='text' class='form-control' name='crew_id[]' value='{$crew}' placeholder='Crew ID' required></div><div class='col-md-2'><button class='btn btn-danger' class='remove_crew'  type='button'><i class='flaticon-delete-1 remove_crew'></i></button></div></div>";
                                                }
                                            @endphp
                                        @endif

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 col-md-6">

                                <label class="main-title">Address:</label>
                                <textarea class="form-control" name="address" id="address" rows="3" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 160px;">{{$project->address}}</textarea>
                            </div>
                            <div class="col-md-1 col-sm-1">
                                <label class="main-title"> files:</label>
                                <div class="form-group " >
                                    @php
                                    //var_dump(json_decode($ticket->original_photoname));
                                    $filename_array = $project->filename;
                                    if(!empty($project->original_filename)){
                                        //var_dump(json_decode($ticket->photoname));
                                        $index = 1;
                                        foreach($project->original_filename as $key=> $file){
                                        echo "<li>&nbsp;&nbsp;&nbsp;&nbsp;<span class='attached-file kt-badge mb-1 kt-badge--success kt-badge--inline kt-badge--pill ' data-container='body' data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='".$file."'"." onClick=fileDownload('".asset("storage/app/public/project/".$filename_array[$key])."')". ">".$index."</span></li>";
                                        $index++;
                                        }
                                    }
                                    @endphp
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-11">
                                <label class="main-title">File uploads:</label>
                                <label class="sub-title">Add documents to backup (estimate, project notes, scope, etc)</label>
                                <div class="form-group file-area fileupload-group"  style='height: 134px !important;'>
                                    <input type="file"  name="fileupload[]"  multiple required>
                                    <p>Drag your files here or click in this area.(images, .doc, .pdf, .xls ... )</p>


                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="reset" class="btn btn-primary" id="submit-project">Update</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
        <div class="col-lg-4">

            <!--Begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title" style="font-size:2rem;">
                            history
                        </h2>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                        <a href="{{url('/project/history/clear')}}" class="btn btn-clean btn-sm btn-bold" >
                            <i class="fa fa-trash-alt"></i> History clear
                        </a>
                    </div>

                </div>
                <div class="kt-portlet__body">


                    <!--begin::Preview-->
                        <div class="kt-demo">
                            <div class="kt-demo__preview">
                                <div class="kt-list-timeline">
                                    <div class="kt-list-timeline__items">
                                        @if (!empty($project_log))
                                            @foreach($project_log as $his)
                                                @if($his->status == 0)
                                                    <div class="kt-list-timeline__item">
                                                        <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                        <span class="kt-list-timeline__icon flaticon2-position kt-font-danger"></span>
                                                        <span class="kt-list-timeline__text">manager: {{$his->user->name}}
                                                            <span class="kt-badge kt-badge--success kt-badge--inline">Item created</span></span>
                                                        <span class="kt-list-timeline__time" style="width:130px;">{{date('F j, Y, g:i a', strtotime($his->created_at))}}</span>
                                                    </div>
                                                @elseif($his->status == 1)
                                                    <div class="kt-list-timeline__item">
                                                        <span class="kt-list-timeline__badge kt-list-timeline__badge--primary"></span>
                                                        <span class="kt-list-timeline__icon flaticon2-note kt-font-primary"></span>
                                                        <span class="kt-list-timeline__text">manager: {{$his->user->name}} <span class="kt-badge kt-badge--info kt-badge--inline">Item updated</span></span>
                                                        <span class="kt-list-timeline__time" style="width:130px;">{{date('F j, Y, g:i a', strtotime($his->created_at))}}</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!--end::Preview-->
                </div>
            </div>

            <!--End:: Portlet-->
        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Client Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name">
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new client</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')


    <!--begin::Page Vendors(used by this page) -->

    <script>
        "user strict";
        var create_project = function(){

            var create_p= $('#create_project');
            var submit_new_project = function(){
                $('#submit-project').click(function(e){
                    console.log('sdfsd');
                    e.preventDefault();
                    var btn = $(this);
                    var form = btn.closest('form');
                    form.validate({
                        rules: {
                            project_name: {
                                required:true,
                            },
                            client_id:{
                                required:true,
                            },
                            project_num:{
                                number: true,
                                required:true,
                            },
                            project_startdate:{
                                required: true,
                            },
                            user_id:{
                                required: true,
                            },
                            manager_id:{
                                required: true,
                            },
                            address:{
                                required: true,
                            },
                            fileupload: {
                                required: true,
                            },


                        }
                    })

                    if (!form.valid()) {
                        return;
                    }

                    form.submit();


                });
            }

            var new_client = function()
            {
                $('#new-client-btn').click(function(){
                    var _this = $(this);
                    var client_modal = $('#new_client');
                    var form = _this.closest('form');
                    var inputs = $('#new_client input');
                    form.validate({
                        rules:{
                            new_client_name:{
                                required:true,
                            },
                        }
                    })

                    if(!form.valid()){
                        return;
                    }
                    _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                    form.ajaxSubmit({
                    url: "{{url('create_client')}}",
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        if(response.success == 'true'){
                            swal.fire("New client has been created!", "Please use new client.","success");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();


                            //location.reload();
                        } else {
                            swal.fire("The client existed with same name!","Please use another name.", "error");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();

                            // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                        }
                    },
                    error: function() {

                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                    }
                });


                })

            }

            var file_change = function(){
                $('.fileupload-group input').change(function () {
                    $('.fileupload-group p').text(this.files.length + " file(s) selected");
                });
            }

            var ticket_option = function(){
                $('#ticket_op').click(function(){
                    _this = $('#ticket_option').val();
                    if(_this == 1){
                        $('#ticket_option').val(0);
                    }   else {
                        $('#ticket_option').val(1);
                    }

                });
            }




            //public function
            return {
                init: function(){
                    ticket_option();
                    submit_new_project();
                    new_client();
                    file_change();
                  }
            };


        }();

        $(document).on('click','#add_crew', function(e){
            e.preventDefault();
            var content = "<div class='row pb-1'><div class='col-md-5'><input type='text' class='form-control' name='crew_name[]' placeholder='Enter crew name ' required></div><div class='col-md-5'><input type='text' class='form-control' name='crew_id[]' placeholder='Enter crew ID' required></div><div class='col-md-2'><button class='btn btn-danger' class='remove_crew'  type='button'><i class='flaticon-delete-1 remove_crew'></i></button></div></div>"

            $("#crew_group").append(content);

        }).on('click','.remove_crew', function(){

                $(this).closest('.row').remove();
        })

        jQuery(document).ready(function(){
            create_project.init();
        });
        $('#kt_select2_10').select2({
            placeholder: "Select an manager",
            minimumResultsForSearch: Infinity
        });

        $('#client_id').select2({
            placeholder: "Select an client",
            minimumResultsForSearch: Infinity
        });

        $('#company_id').select2({
            placeholder: "Select an company",
            minimumResultsForSearch: Infinity
        });

        function fileDownload(url){
        window.URL.revokeObjectURL(url);

        swal.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: false,

            // confirmButtonText: 'Yes, delete it!'
            title: 'Do you want to download file?',
            html: "<a  href='"+url+"' target='_blank' >Preview file.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageAlt: 'Custom image',
            animation: true
        });

    }


    function active(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to change this status!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, change it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}',
                }
            });
            jQuery.ajax({
                url: "{{ url('/change_p_status') }}",
                method: 'post',
                data: {
                    record_id: record_id,

                },
                success: function(result){

                    var element =  $("#p_status").children();
                    if(result.status == 1){
                        console.log(1);
                        element.text('Active');
                        element.removeClass('kt-badge--danger').addClass('kt-badge--primary');

                    } else {
                        element.text('Draft');
                        element.removeClass('kt-badge--primary').addClass('kt-badge--danger');
                    }


                    swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'The status has been updated.',
                    'success'
                    )


                }
            });


        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'You have cancelled the modification of the status',
            'error'
            )
        }
        })

        }


    </script>


    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif

    @if($errors->any())
        <script>
            swal.fire("{{ implode('', $errors->all(':message')) }}", "Please confirm it.","error");
        </script>
    {{ implode('', $errors->all('<div>:message</div>')) }}
    @endif




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


