@extends('layout.master')
@section('custom-css')
<link href="{{asset('public/custom-css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/custom-css/order_list.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom-js')

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Change Order </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Change Order List</span>
    </div>
</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">

        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/order/create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Change Order
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">

        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                           <i  class="fa fa-ticket-alt"></i> &nbsp;&nbsp;&nbsp;Icon Tabs
                        </h2>
                    </div>
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                           <i  class="fa fa-money-check-alt"></i> Total Open: &nbsp;&nbsp;&nbsp;{{$total_open}}
                        </h2>
                        <h2 class="kt-portlet__head-title" style="padding-left:20px;">
                            <i  class="flaticon2-file"></i> Total Paid: &nbsp;&nbsp;&nbsp;{{$total_paid}}
                         </h2>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-primary" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tabs_8_1" role="tab"><i class="fa fa-envelope-open"></i> Open</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_8_2" role="tab"><i class="fa fa-envelope"></i> All Change Orders</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_tabs_8_1" role="tabpanel">
                            <div class="row">
                                <div class=" col-md-12">
                                    <table  id="example" width="100%" class="table border-table display">
                                        <thead>
                                            <tr>
                                                <th title="Field #1" class="text-right">Status</th>
                                                <th title="Field #2" class="text-right">Subject</th>
                                                <th title="Field #3" class="text-right">Project name</th>
                                                <th title="Field #3" class="text-right">Company</th>
                                                <th title="Field #4" class="text-right">Order Number</th>
                                                <th title="Field #5" class="text-right">Client Number</th>
                                                <th title="Field #6" class="text-right">Issue Date</th>
                                                <th title="Field #7" class="text-right" >Balance</th>
                                                <th title="Field #8" class="text-right" >Approved</th>
                                                <th title="Field #9" class="text-right" >Complete</th>
                                                <th title="Field #10" class="text-right" >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            @if (!empty($orders_draft))
                                                @foreach($orders_draft as $order)
                                                <tr>
                                                    <td  class="text-right">

                                                        @if($order->status == 0)
                                                            <span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill'>Open</span>
                                                        @elseif($order->status == 1)
                                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>Revise</span>
                                                        @elseif($order->status == 2)
                                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>Approved</span>
                                                        @elseif($order->status == 3)
                                                        <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Closed</span>
                                                        @endif

                                                    </td>
                                                    <td  class="text-right">{{$order->order_subject}}</td>
                                                    <td  class="text-right">{{$order->get_project->project_name}}</td>
                                                    <td  class="text-right">{{$order->get_project->company->name}}</td>
                                                    <td  class="text-right">{{$order->order_num}}</td>
                                                    <td  class="text-right">{{$order->client_co_num}}</td>
                                                    <td  class="text-right">{{$order->order_date}}</td>

                                                    <td  class="text-right">@convert($order->balance)</td>
                                                    <td  class="text-right">
                                                        @if($order->approved == 1)
                                                        <span class="hidden_span">Yes</span>
                                                        @else
                                                        <span class='hidden_span'>no</span>
                                                        @endif
                                                        <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox" name="order_type" value="1"  @if($order->approved == 1) checked @endif  disabled>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td  class="text-right">
                                                        @if($order->complete == 1)
                                                        <span class="hidden_span">Yes</span>
                                                        @else
                                                        <span class='hidden_span'>no</span>
                                                        @endif
                                                        <label class="kt-checkbox kt-checkbox--success">
                                                            <input type="checkbox" name="order_type" value="1"  @if($order->complete == 1) checked @endif  disabled>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td class="text-right" style="width:208px">
                                                        <a href="{{url('/order/'.$order->id.'/view')}}" type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record"><i class="fa fa-eye"></i> </a>

                                                    </td>
                                                </tr>
                                                @endforeach

                                            @endif


                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th title="Field #1" class="text-right">Status</th>
                                                <th title="Field #2" class="text-right">Subject</th>
                                                <th title="Field #3" class="text-right">Project name</th>
                                                <th title="Field #3" class="text-right">Company</th>
                                                <th title="Field #4" class="text-right">Order Number</th>
                                                <th title="Field #5" class="text-right">Client Number</th>
                                                <th title="Field #6" class="text-right">Issue Date</th>
                                                <th title="Field #7" class="text-right" >Balance</th>
                                                <th title="Field #8" class="text-right" >Approved</th>
                                                <th title="Field #9" class="text-right" >Complete</th>
                                                <th title="Field #10" class="text-right" >Action</th>
                                            </tr>
                                        </tfoot>

                                    </table>


                                </div>
                            </div>


                        </div>
                        <div class="tab-pane" id="kt_tabs_8_2" role="tabpanel">
                            <div class="row">
                                <div class=" col-md-12">
                                    <table  id="example1" width="100%" class="table border-table display">
                                        <thead>
                                            <tr>
                                                <th title="Field #1" class="text-right">Status</th>
                                                <th title="Field #2" class="text-right">Subject</th>
                                                <th title="Field #3" class="text-right">Project name</th>
                                                <th title="Field #3" class="text-right">Company</th>
                                                <th title="Field #4" class="text-right">Order Number</th>
                                                <th title="Field #5" class="text-right">Client Number</th>
                                                <th title="Field #6" class="text-right">Issue Date</th>
                                                <th title="Field #7" class="text-right" >Balance</th>
                                                <th title="Field #8" class="text-right" >Approved</th>
                                                <th title="Field #9" class="text-right" >Complete</th>
                                                <th title="Field #10" class="text-right" >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            @if (!empty($orders))
                                                @foreach($orders as $order)
                                                <tr>

                                                    <td  class="text-right">
                                                        @if($order->status == 0)
                                                            <span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill'>Open</span>
                                                        @elseif($order->status == 1)
                                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>Revise</span>
                                                        @elseif($order->status == 2)
                                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>Approved</span>
                                                        @elseif($order->status == 3)
                                                        <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Closed</span>
                                                        @endif
                                                    </td>
                                                    <td  class="text-right">{{$order->order_subject}}</td>
                                                    <td  class="text-right">{{$order->get_project->project_name}}</td>
                                                    <td  class="text-right">{{$order->get_project->company->name}}</td>
                                                    <td  class="text-right">{{$order->order_num}}</td>
                                                    <td  class="text-right">{{$order->client_co_num}}</td>
                                                    <td  class="text-right">{{$order->order_date}}</td>
                                                    <td  class="text-right">@convert($order->balance)</td>
                                                    <td  class="text-right" >
                                                        @if($order->approved == 1)
                                                        <span class="hidden_span">Yes</span>
                                                        @else
                                                        <span class='hidden_span'>no</span>
                                                        @endif
                                                        <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox" name="order_type" value="1"  @if($order->approved == 1) checked @endif  disabled>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td  class="text-right">
                                                        @if($order->complete == 1)
                                                        <span class="hidden_span">Yes</span>
                                                        @else
                                                        <span class='hidden_span'>no</span>
                                                        @endif
                                                        <label class="kt-checkbox kt-checkbox--success"  >
                                                            <input type="checkbox" name="order_type" value="1"  @if($order->complete == 1) checked @endif  disabled>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td class="text-right" style="width:208px">
                                                        <a href="{{url('/order/'.$order->id.'/view')}}" type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record"><i class="fa fa-eye"></i> </a>

                                                    </td>
                                                </tr>
                                                @endforeach

                                            @endif


                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th title="Field #1" class="text-right">Status</th>
                                                <th title="Field #2" class="text-right">Subject</th>
                                                <th title="Field #3" class="text-right">Project name</th>
                                                <th title="Field #3" class="text-right">Company</th>
                                                <th title="Field #4" class="text-right">Order Number</th>
                                                <th title="Field #5" class="text-right">Client Number</th>
                                                <th title="Field #6" class="text-right">Issue Date</th>
                                                <th title="Field #7" class="text-right" >Balance</th>
                                                <th title="Field #8" class="text-right" >Approved</th>
                                                <th title="Field #9" class="text-right" >Complete</th>
                                                <th title="Field #10" class="text-right" >Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--dashed"></div>

                </div>
            </div>

            <!--begin: Datatable -->



            <!--end: Datatable -->
        </div>


    </div>

</div>
<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')<script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script> 
<script src="{{asset('public/custom-js/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/uttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/buttons.print.min.js')}}" type="text/javascript"></script>  
@if(session()->has('error'))
    <script>
        swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
    </script>
@endif

@if(session()->has('success'))
    <script>
        swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
    </script>
@endif
@endsection


