@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />
<style>
    #record_payment{
        top:30%;
    }
</style>
@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Order </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">New Order</span>
    <a href="{{url('/new_ticket')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New
        </a>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch" required>
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                            Order
                        </h2>
                    </div>
                </div>

                <!--begin::Form-->
                <div class="container-fuild pt-3 pl-3">
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content Head -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">
								<h3 class="kt-subheader__title">
									Order View
								</h3>
								<span class="kt-subheader__separator kt-subheader__separator--v"></span>
								<div class="kt-subheader__group" id="kt_subheader_search">
									<span class="kt-subheader__desc" id="kt_subheader_total">
                                        Submit, Update, Delete ticket.
										 </span>
								</div>
							</div>
							{{-- <div class="kt-subheader__toolbar">
								<a onclick="goBack()" class="btn btn-default btn-bold">
									Back </a>
							</div> --}}
						</div>

						<!-- end:: Content Head -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--Begin:: Portlet-->
							<div class="kt-portlet">
								<div class="kt-portlet__body">
									<div class="kt-widget kt-widget--user-profile-3">
										<div class="kt-widget__top">
											{{-- <div class="kt-widget__media">
												<img src="./assets/media/users/100_12.jpg" alt="image">
											</div> --}}
											<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-bolder kt-font-light kt-hidden">
												JM
											</div>
											<div class="kt-widget__content">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="kt-widget__head">
                                                            <div class="kt-widget__user">
                                                                <a  class="kt-widget__username">
                                                                    <h2>Order &NonBreakingSpace;{{$order->order_num." - ". $order->order_subject}}</h2>
                                                                </a>

                                                            </div>

                                                        </div>
                                                        <div class="kt-widget__head">
                                                            <div class="kt-widget__user">
                                                                <a  class="kt-widget__username">
                                                                    @if($order->status == 0)
                                                                    <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" >  Open
                                                                    @elseif($order->status == 1)
                                                                    <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > Revised
                                                                    @elseif($order->status == 2)
                                                                    <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > Approved
                                                                    @elseif($order->status == 3)
                                                                    <h4>Status:  &NonBreakingSpace; <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" >Closed
                                                                    @endif
                                                                    </span>
                                                                    @if ($order->approved == 1)
                                                                    |&nbsp;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > Approved</span>
                                                                    @endif
                                                                    @if ($order->complete == 1)
                                                                    |&nbsp;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > Complete</span>
                                                                    @endif
                                                                    </h4>

                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__head">
                                                            <div class="kt-widget__user">
                                                                <a  class="kt-widget__username">
                                                                    <h4> Latest Activity:  &NonBreakingSpace;

                                                                            @if($latest_activity->history_status == 0)
                                                                                Item opened
                                                                            @elseif($latest_activity->history_status == 1)
                                                                                Item revised
                                                                            @elseif($latest_activity->history_status == 2)
                                                                                Item approved
                                                                            @elseif($latest_activity->history_status == 3)
                                                                                Item closed
                                                                            @elseif($latest_activity->history_status == 4)
                                                                                Item changed
                                                                            @endif



                                                                    </h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__subhead">
                                                            <a ><i class="flaticon2-new-email"></i>{{$latest_activity->get_user->name}}</a>
                                                            <a ><i class="flaticon-calendar-with-a-clock-time-tools"></i>
                                                                @if (!empty($latest_activity))
                                                                    @if($latest_activity->created == 1)
                                                                        {{date('F j, Y, g:i a',strtotime($latest_activity->created_at))}}
                                                                    @else
                                                                    {{date('F j, Y, g:i a',strtotime($latest_activity->updated_at))}}
                                                                    @endif

                                                                @endif

                                                        </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-4 text-right" style="border-right:2px solid #c1c1c1;">
                                                                <h3>From  </h3>
                                                                <h3>To  </h3>
                                                            </div>
                                                            <div class="col-md-8 text-left">
                                                                <h3>{{$mycom}} </h3>
                                                                <h3>{{$order->get_project->company->name}} </h3>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>


											</div>
										</div>

									</div>
								</div>
							</div>

							<!--End:: Portlet-->
							<div class="row">
								<div class="col-xl-8 col-sm-12">
									<!--Begin:: Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h2 class="kt-portlet__head-title" style="font-size:2rem;">
													Change Order Detail
												</h2>
											</div>
											<div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-clean btn-sm btn-bold" data-toggle="dropdown">
													<i class="flaticon2-gear"></i>Status Setting
												</a>
												<div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">

													<!--begin::Nav-->
													<ul class="kt-nav">
														<li class="kt-nav__head">
															Status Settings
															<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
														</li>
														<li class="kt-nav__separator"></li>
														<li class="kt-nav__item">
															<a href="{{url('/order/'.$order->id.'/submit')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-new-email"></i>
																<span class="kt-nav__link-text">Opened</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="{{url('/order/'.$order->id.'/revise')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-attention"></i>
																<span class="kt-nav__link-text">Revised</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="{{url('/order/'.$order->id.'/approve')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-rocket-2"></i>
																<span class="kt-nav__link-text">Approved</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="{{url('/order/'.$order->id.'/close')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon fa fa-door-closed"></i>
																<span class="kt-nav__link-text">Closed</span>
															</a>
														</li>
														<li class="kt-nav__separator"></li>

													</ul>

													<!--end::Nav-->
												</div>
                                            </div>
										</div>
										<div class="kt-form kt-form--label-right">
											<div class="kt-portlet__body">
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Change Order subject:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{$order->order_subject}}</h4></span>
													</div>
                                                </div>
                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Order Change #:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{$order->order_num}}</h4></span>
													</div>
                                                </div>
                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Client CO#:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{$order->client_co_num}}</h4></span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Project name:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{$order->get_project->project_name}}</h4></span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Date:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$order->order_date}}</h4></span>
													</div>
                                                </div>
                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Scope of Work:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$order->scope}}</h4></span>
													</div>
                                                </div>
                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Exclusion:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$order->exclusion}}</h4></span>
													</div>
                                                </div>

                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>RFP:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder">
                                                            @php

                                                            if(!empty($order->efp_originalname)){

                                                                //var_dump(json_decode($ticket->photoname));
                                                                $index = 1;
                                                                $original_name = $order->efp_filename;

                                                                foreach($order->efp_originalname as $key=> $filename){

                                                                    echo "<span class='attached-file kt-badge mb-1 kt-badge--success kt-badge--inline kt-badge--pill '"." onClick=fileDownload('".asset("storage/app/public/rfp/".$filename)."')". ">".$original_name[$key]."</span> &nbsp;&nbsp;";
                                                                    $index++;
                                                                }

                                                            }
                                                            @endphp
                                                        </span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Type:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder">
                                                            <h4>
                                                                @if ($order->order_type == 1)
                                                                    Time & Materials
                                                                @elseif($order->order_type ==2)
                                                                    Lump Sum(Rates)
                                                                @elseif($order->order_type ==3)
                                                                    Lump Sum(Total)
                                                                @endif
                                                            </h4>
                                                        </span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Tickets:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder">
                                                            @foreach ($order->ticketid_collection as $ticket)
                                                                @php
                                                                    $ticket_model = App\Ticket::find($ticket);
                                                                @endphp
                                                                 <span ><h4>{{$ticket_model->ticket_subject}}</h4></span>
                                                            @endforeach
														</span>
													</div>
												</div>

											</div>

                                        </div>
                                        <div class="kt-portlet__foot">

                                            <div class="kt-form__actions kt-space-between" style="text-align:right;display:block;">

                                                <a href="{{url('/order/'.$order->id.'/oapprove')}}" class="btn btn-label-primary btn-sm btn-bold btn-primary">Work Approve</a>
                                                <a href="{{url('/order/'.$order->id.'/complete')}}" class="btn btn-label-primary btn-sm btn-bold btn-primary">Work Complete</a>
                                                <a href="{{url('/order/'.$order->id.'/edit')}}" class="btn btn-label-primary btn-sm btn-bold btn-primary">Edit</a>
                                                <a onclick="deleteOrder({{$order->id}})"  class="btn btn-label-primary btn-sm btn-bold btn-primary">Delete</a>
                                                |
                                                <a href="{{url('/order/'.$order->id.'/print')}}" class="btn btn-label-brand btn-sm btn-bold"><i class="fa fa-print"></i></a>
                                                <a  data-toggle='modal' data-target = '#record_payment'  class="btn btn-label-brand btn-sm btn-bold"><i class="fa fa-dollar-sign"></i></a>
                                            </div>
                                            <div class="kt-form__actions kt-space-between pt-3" style="text-align:right;display:block;">
                                                <h3>Blance: <span id="order_balance">{{$order->balance}}</span></h3>
                                            </div>
                                        </div>


									</div>

									<!--End:: Portlet-->
								</div>
								<div class="col-xl-4 col-sm-12">

									<!--Begin:: Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h2 class="kt-portlet__head-title" style="font-size:2rem;">
                                                    history
                                                </h2>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">

												<a href="{{url('/order/'.$order->id.'/history/clear')}}" class="btn btn-clean btn-sm btn-bold" >
													<i class="fa fa-trash-alt"></i> History clear
												</a>
                                            </div>

										</div>
										<div class="kt-portlet__body">
											<div class="tab-content ">
                                                 <!--begin::Preview-->
                                                    <div class="kt-demo">
                                                        <div class="kt-demo__preview">
                                                            <div class="kt-list-timeline">
                                                                <div class="kt-list-timeline__items">
                                                                    @if ( !empty($order_history))
                                                                        @foreach($order_history as $his)

                                                                                <div class="kt-list-timeline__item">
                                                                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                                                        @if($his->history_status == 0)
                                                                                        <span class="kt-list-timeline__icon flaticon2-new-email kt-font-primary"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->get_user->name}}
                                                                                        <span class="kt-badge kt-badge--primary kt-badge--inline">
                                                                                            Item opened
                                                                                        @elseif($his->history_status == 1)
                                                                                        <span class="kt-list-timeline__icon flaticon2-attention kt-font-warning"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->get_user->name}}
                                                                                        <span class="kt-badge kt-badge--warning kt-badge--inline">
                                                                                            Item revised
                                                                                        @elseif($his->history_status == 2)
                                                                                        <span class="kt-list-timeline__icon flaticon2-rocket-2 kt-font-success"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->get_user->name}}
                                                                                        <span class="kt-badge kt-badge--success kt-badge--inline">
                                                                                        Item approved
                                                                                        @elseif($his->history_status == 3)
                                                                                        <span class="kt-list-timeline__icon
                                                                                        fa fa-door-closed kt-font-success"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->get_user->name}}
                                                                                        <span class="kt-badge kt-badge--success kt-badge--inline">
                                                                                        Item closed
                                                                                        @elseif($his->history_status == 4)
                                                                                        <span class="kt-list-timeline__icon flaticon2-note  kt-font-primary"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->get_user->name}}
                                                                                        <span class="kt-badge kt-badge--primary kt-badge--inline">
                                                                                        Item updated
                                                                                        @endif
                                                                                    </span></span>
                                                                                    <span class="kt-list-timeline__time" style="width:145px;">{{date('F j, Y, g:i a', strtotime($his->created_at))}}</span>
                                                                                </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <!--end::Preview-->




											</div>
										</div>
									</div>

									<!--End:: Portlet-->
								</div>
							</div>
						</div>

						<!-- end:: Content -->
					</div>



                </div>


                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Client Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new client</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<!-- end:: Content -->
{{-- Start : modal --}}
<div class="modal fade" id="record_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update balance</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>
                @csrf
                    <div class="form-group">
                        <label for="new-client-name" class="form-control-label">Blance:</label>
                        <input type="text" name='balance' class="form-control text-right" value="{{$order->balance}}">
                    </div>
                    <input type="hidden" name='id' class="form-control text-right" value="{{$order->id}}">
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="record-payment-btn" class="btn btn-primary">Update</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
{{-- End : modal --}}
{{-- Start : modal --}}
<form action="{{url('/order/'.$order->id.'/delete')}}" method="get" id="delete_form">
 @csrf

</form>
{{-- End : modal --}}
@endsection

@section('page-js')


    <!--begin::Page Vendors(used by this page) -->

    <script>
        "user strict";
        var create_project = function(){

            var update_payment = function()
            {
                $('#record-payment-btn').click(function(){
                    var _this = $(this);
                    var client_modal = $('#record_payment');
                    var form = _this.closest('form');
                    var inputs = $('#record_payment input');
                    form.validate({
                        rules:{
                            balance:{
                                required:true,
                                number:true,
                            },
                        }
                    })

                    if(!form.valid()){
                        return;
                    }
                    _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                    form.ajaxSubmit({
                    url: "{{url('/order/payment')}}",
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        if(response.success == 'true'){
                            swal.fire("The balance has been updated!", "Please check the balance.","success");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();
                            $('#order_balance').text(response.balance);
                            //location.reload();
                        } else {
                            swal.fire("The network error!","Please check  your network.", "error");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();

                            // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                        }
                    },
                    error: function() {

                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            swal.fire("The network error!","Please check  your network.", "error");


                    }
                });


                })

            }

            var file_change = function(){
                $('.fileupload-group input').change(function () {
                    $('.fileupload-group p').text(this.files.length + " file(s) selected");
                });
            }

            var ticket_option = function(){
                $('#ticket_op').click(function(){
                    _this = $('#ticket_option').val();
                    if(_this == 1){
                        $('#ticket_option').val(0);
                    }   else {
                        $('#ticket_option').val(1);
                    }

                });
            }
            //public function
            return {
                init: function(){
                    ticket_option();
                    update_payment();
                    file_change();
                }
            };


        }();
        jQuery(document).ready(function(){
            create_project.init();
        });

        $('#project_id').select2({
            placeholder: "Select an Project Name",
            minimumResultsForSearch: Infinity
        });


    function fileDownload(url){
        window.URL.revokeObjectURL(url);

        swal.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: false,

            // confirmButtonText: 'Yes, delete it!'
            title: 'Do you want to download file?',
            html: "<a  href='"+url+"' target='_blank' >Preview file.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageAlt: 'Custom image',
            animation: true
        });
    }



    function deleteOrder(id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $('#delete_form').submit();

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This order is safe',
                'error'
                )
            }
        })

    }


    </script>


    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


