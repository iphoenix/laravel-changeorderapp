@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Order </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">New Order</span>
        <a href="{{url('/order')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Order list
        </a>

    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            New Change Order
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="create_project" action="{{url('/order')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group row divide pb-4">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="main-title">Change Order Number:</label>
                                <input type="text" class="form-control" name="order_num" placeholder="Enter Change Order Number" required>
                                <span class="form-text text-muted">Please enter Change Order Number</span>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="main-title">Client CO Number:</label>
                                <input type="text" class="form-control" name="client_co_num" placeholder="Enter Client CO Number" required>
                                <span class="form-text text-muted">Please Client CO Number</span>
                            </div>
                        </div>

                        <div class="form-group row divide pb-4">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="main-title">Subject:</label>
                                <input type="text" class="form-control" name="order_subject" placeholder="Enter Subject" required>
                                <span class="form-text text-muted">Please enter Subject</span>
                            </div>
                        </div>


                        <div class="form-group row divide pb-4">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label class="main-title">Project:</label>
                                   <div class="input-group">
                                        <select class="form-control kt-select2" name="project_id" id="project_id">
                                            <option></option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}">{{$project->project_name}}</option>
                                            @endforeach
                                        </select>
                                   </div>
                                    <span class="form-text text-muted">Please select a Project Name</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">Date:</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="order_date" readonly placeholder="Enter optional end date" id="kt_datepicker_2" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please enter Date</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="main-title">Upload RFP</label>
                                <div class="form-group file-area fileupload-group" style="height:50px;">
                                    <input type="file"  name="photos[]" multiple required>
                                    <p>Drag your files here.</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row divide pb-4">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-gropu">
                                    <label class="main-title">Scope of Work:</label>
                                    <textarea class="form-control" name="scope" id="address" rows="3" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 130px;"></textarea>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-gropu">
                                    <label class="main-title">Exclusion:</label>
                                    <textarea class="form-control" name="exclusion" id="address" rows="3" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 130px;"></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="form-group row divide pb-4">
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label class="main-title">Type:</label>
                                    <div class="kt-checkbox-inline">
                                        <label class="kt-checkbox kt-checkbox--success">
                                            <input type="radio" name="order_type" value="1"> Time & Materials
                                            <span></span>
                                        </label>
                                        <label class="kt-checkbox kt-checkbox--success">
                                            <input type="radio" name="order_type" value="2"> Lump Sum (Rates)
                                            <span></span>
                                        </label>
                                        <label class="kt-checkbox kt-checkbox--brand">
                                            <input type="radio" name="order_type" value="3"> Lump Sum (Total)
                                            <span></span>
                                        </label>

                                    </div>

                                </div>

                           </div>
                        </div>
                        <div class="form-group row divide pb-4">
                            <div class="col-md-6">
                                <label class="main-title">Add Tickets:</label>
                                <select class="form-control kt-select2" id="multiple" name="ticketid_collection[]" multiple="multiple" placeholder="this">
                                    @foreach($tickets as $ticket)
                                        <option value="{{$ticket->id}}">{{$ticket->ticket_num}}-{{$ticket->ticket_subject}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="reset" class="btn btn-primary" id="submit-project">Save</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
</div>

<!-- end:: Content -->
@endsection

@section('page-js')


    <!--begin::Page Vendors(used by this page) -->

    <script>
        "user strict";
        var create_project = function(){

            var create_p= $('#create_project');
            var submit_new_project = function(){
                $('#submit-project').click(function(e){
                    console.log('sdfsd');
                    e.preventDefault();
                    var btn = $(this);
                    var form = btn.closest('form');
                    form.validate({
                        rules: {
                            order_num: {
                                required:true,

                            },
                            client_co_num:{
                                required:true,
                            },
                            project_id:{
                                number: true,
                                required:true,
                            },
                            order_date:{
                                required: true,
                            },
                            scope:{
                                required: true,
                            },
                            exclusion:{
                                required: true,
                            },
                            order_type:{
                                number: true,
                                required: true,
                            },
                            ticketid_collection:{
                                required: true,
                                number: true,
                            }


                        }
                    })

                    if (!form.valid()) {
                        return;
                    }

                    form.submit();


                });
            }

            var new_client = function()
            {
                $('#new-client-btn').click(function(){
                    var _this = $(this);
                    var client_modal = $('#new_client');
                    var form = _this.closest('form');
                    var inputs = $('#new_client input');
                    form.validate({
                        rules:{
                            new_client_name:{
                                required:true,
                            },
                        }
                    })

                    if(!form.valid()){
                        return;
                    }
                    _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                    form.ajaxSubmit({
                    url: "{{url('create_client')}}",
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        if(response.success == 'true'){
                            swal.fire("New client has been created!", "Please use new client.","success");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();


                            //location.reload();
                        } else {
                            swal.fire("The client existed with same name!","Please use another name.", "error");
                            _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            client_modal.modal('hide');
                            form[0].reset();

                            // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                        }
                    },
                    error: function() {

                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                    }
                });


                })

            }

            var file_change = function(){
                $('.fileupload-group input').change(function () {
                    $('.fileupload-group p').text(this.files.length + " file(s) selected");
                });
            }

            var ticket_option = function(){
                $('#ticket_op').click(function(){
                    _this = $('#ticket_option').val();
                    if(_this == 1){
                        $('#ticket_option').val(0);
                    }   else {
                        $('#ticket_option').val(1);
                    }

                });
            }

            var ticket_list = function(){

                $('#project_id').on('select2:select', function(e){
                    var project_id = $('#project_id').val();


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}',
                        }
                    });
                    jQuery.ajax({
                        url: "{{ url('/') }}/order/"+project_id+"/tickets",
                        method: 'post',
                        data: {
                            project_id: project_id,

                        },
                        success: function(result){
                            if(result.success){
                                var element='';
                                var tickets = result.tickets;
                                let reformattedArray = tickets.map(obj => {
                                    element += "<option value='"+obj.id+"'>"+obj.ticket_num+"-"+obj.ticket_subject+"</option>";
                                    // let rObj = {}
                                    // rObj[obj.key] = obj.value
                                    console.log(element);
                                })
                                $('#multiple').empty().append(element);


                            } else {
                                console.log('false');

                            }


                        }
                    });
                });

            }




            return {
                init: function(){
                    ticket_option();
                    submit_new_project();
                    new_client();
                    file_change();
                    ticket_list();
                }
            };


        }();
        jQuery(document).ready(function(){
            create_project.init();
        });

        $('#project_id').select2({
            placeholder: "Select an Project Name",
            minimumResultsForSearch: Infinity
        });

        $("#multiple").select2({minimumResultsForSearch: Infinity});

$(function(){
    $('#project_id').on('select2:select', function(e){
        var option = $('#project_id').find(':selected').attr('data-option');
        if(option == 1){
            $("#advanced_form").fadeIn();
        }  else {
            $("#advanced_form").fadeOut();
        }
    });
})
    </script>


    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


