@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />
<style>
    .num-mark{
        margin-right: 28px !important;
        margin-top: 25px !important;
    }
</style>
@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Timesheet </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">New Timesheet</span>
        {{-- <a href="{{url('/order')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Order list
        </a> --}}

    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid " id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            New Timesheet
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="create_project" action="{{url('/timesheet')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}"/>
                    <div class="kt-portlet__body ">
                        <div class="form-group row divide pb-4">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="main-title">Project:</label>
                                   <div class="input-group">
                                        <select class="form-control kt-select2" name="project_id" id="project_id" required>
                                            <option></option>
                                            @foreach($projects as $project)
                                                <option data-option="{{$project->ticket_option}}" value="{{$project->id}}">{{$project->project_name}}</option>
                                            @endforeach
                                        </select>
                                   </div>
                                    <span class="form-text text-muted">Please select a Project Name</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">Work Date:</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="work_date" readonly placeholder="Enter optional end date" id="kt_datepicker_2" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please enter Date</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row divide pb-4">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">Start Time:</label>

                                        <div class="input-group timepicker">
                                            <input class="form-control" name="start_time" id="kt_timepicker_1" readonly placeholder="Select time" type="text" required/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>

                                    <span class="form-text text-muted">Please enter start time</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">End date:</label>

                                        <div class="input-group timepicker">
                                            <input class="form-control" name="end_time" id="kt_timepicker_2" readonly placeholder="Select time" type="text" required/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>

                                    <span class="form-text text-muted">Please enter end time</span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row divide ">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="main-title">Add Break:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
                                            <label>
                                                <input type="checkbox" id="add_break" name="">
                                                <span></span>
                                            </label>
                                            <input type="hidden" name="break" id="break" value="0" >
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row divide ">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="main-title">Add Overtime:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
                                            <label>
                                                <input type="checkbox" id="add_overtime" name="">
                                                <span></span>
                                            </label>
                                            <input type="hidden" name="overtime" id="overtime" value="0">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row  " >
                            <div class="col-md-6" id="labor_section">
                                <label class="main-title">Workers:</label>
                                <div class="form-group row  labor_group">
                                    <div class="col-md-1 color-stick text-right">
                                        <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="sub-title">worker</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2" name="worker[]" id="worker" required>

                                                @foreach($workers as $worker)
                                                    <option  value="{{$worker->id}}">{{$worker->worker_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <label class="sub-title">Hours</label>
                                        <div class="input-group date">
                                            <input type="number" class="form-control labor_hours" name="hours[]" value="0"  placeholder="Hours (per person)"  required/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    hrs
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div  class="col-md-6"></div>
                            <div class="col-md-6 pb-4 text-right">
                                <button type="button" id="add_labor" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Labor</button>
                                <button type="button" id="remove_labor" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Labor</button>
                            </div>
                        </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="reset" class="btn btn-primary" id="submit-project">Save</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
</div>

<!-- end:: Content -->
@endsection

@section('page-js')
    <script src="{{asset('public/custom-js/timesheet_new.js')}}" type="text/javascript"></script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
    <!--end::Page Scripts -->
@endsection


