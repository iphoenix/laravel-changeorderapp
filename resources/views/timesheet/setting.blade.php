@extends('layout.master')

<style>
    .attachment{
        cursor: pointer;
    }
    #new_client{
        top:15% !important;
    }
    #new_company{
        top:15% !important;
    }
</style>
@section('custom-css')

@endsection
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Timesheet </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Timesheet Setting</span>

    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            {{-- <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span> --}}
            {{-- <h3 class="kt-portlet__head-title">
                HTML Table
                <small>Datatable initialized from HTML table</small>
            </h3> --}}
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/timesheet/create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Timesheet
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div class="kt-portlet__body kt-portlet__body--fit">
        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head row">
                        <div class="col-md-6 text-left mt-3">
                            <h2><i class="fa fa-user-alt"></i>&nbsp;&nbsp;&nbsp;Workers</h2>
                        </div>
                        <div class="col-md-6 text-right mt-3">
                            <button type="button" data-toggle='modal' data-target="#new_client" id="add_client" class="btn btn-primary btn-sm btn-elevate  btn-icon"><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                        <div class="kt-section">

                            <div class="kt-section__content">
                                <table class="table" id="client" width="100%">
                                    <thead>
                                        <tr>
                                            <th title="Field #1">worker name</th>
                                            <th title="Field #2">Created time</th>
                                            <th title="Field #3" style="width:30px !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($workers))
                                            @foreach($workers as $worker)
                                                <tr>
                                                    <td>{{$worker->worker_name}}</td>
                                                    <td>{{$worker->created_at}}</td>
                                                    <td>
                                                        <a onclick="delete_client({{$worker->id}})" id="{{'delete_client'.$worker->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
                                                        <i class="fa fa-trash-alt"></i>
                                                        </a>

                                                        <a onclick="edit_worker({{$worker->id}})" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                </div>
            </div>

        </div>


    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Worker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New worker Name:</label>
                        <input type="text" name='name' class="form-control" id="new_client_name" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new labor</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!--begin::new_company Modal-->
<div class="modal fade" id="worker_update_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Update Worker name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <input type="hidden" id="worker_id" name="worker_id" value="">
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label"> Name:</label>
                        <input type="text" name='name' id="worker_name" class="form-control text-right" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="worker_update_btn" class="btn btn-primary">Update Worker Name</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end::Modal-->


<!-- end:: Content -->
@endsection

@section('page-js')


    <script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/datatables.bootstrap4.min.js')}}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $("#client").DataTable({
                aaSorting: [],
                responsive: true,

                columnDefs: [
                {
                    responsivePriority: 1,
                    targets: 0
                },
                {
                    responsivePriority: 2,
                    targets: -1
                }
                ]
            });

            $("#company").DataTable({
                aaSorting: [],
                responsive: true,

                columnDefs: [
                {
                    responsivePriority: 1,
                    targets: 0
                },
                {
                    responsivePriority: 2,
                    targets: -1
                }
                ]
            });

            $(".dataTables_filter input")
                .attr("placeholder", "Search here...")
                .css({
                width: "100px",
                display: "inline-block"
                });

            $('[data-toggle="tooltip"]').tooltip();
        });

        "use strict";
    // Class definition

    var KTDatatableHtmlTableDemo = function() {


        var new_client = function()
                {
                    $('#new-client-btn').click(function(){
                        var _this = $(this);
                        var client_modal = $('#new_client');
                        var form = _this.closest('form');
                        var inputs = $('#new_client input');
                        form.validate({
                            rules:{
                                new_client_name:{
                                    required:true,
                                },
                            }
                        })

                        if(!form.valid()){
                            return;
                        }
                        _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                        form.ajaxSubmit({
                        url: "{{url('/worker/create')}}",
                        success: function(response, status, xhr, $form) {
                            // similate 2s delay
                            if(response.success){
                                swal.fire("New Worker has been created!", "Please use new Worker.","success");
                                _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                client_modal.modal('hide');
                                form[0].reset();
                                setTimeout(function(){ location.reload(true); }, 1000);



                                //location.reload();
                            } else {
                                swal.fire("The Worker existed with same name!","Please use another name.", "error");
                                _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                client_modal.modal('hide');
                                form[0].reset();

                                // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                            }
                        },
                        error: function() {

                                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                        }
                    });


                    })

                }
        var update_worker = function()
        {
            $('#worker_update_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#worker_update_form');
                var form = _this.closest('form');
                var inputs = $('input');
                var btn = $('#worker_update_btn');
                form.validate({
                    rules:{
                        name:{
                            required:true,
                        },
                        worker_id:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/worker/update')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success){
                        swal.fire("Worker name has been updated!", "Please make sure it.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The worker name existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }

        var stop_enter = function (){
            $('form input').keydown(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();

                    swal.fire("Please click submit button!","- Do not use enter key-.", "error");
                    return false;
                }
            });
        }


        return {
            // Public functions
            init: function() {
                stop_enter();
                new_client();
                update_worker();

            },
        };
    }();

    jQuery(document).ready(function() {
        KTDatatableHtmlTableDemo.init();
    });

    function edit_worker(worker_id){

        $('#worker_id').val(worker_id);
        var id = '#delete_client'+worker_id;
        $('#worker_name').val($(id).parent().prev().prev().text());
        $('#worker_update_form').modal('show');
    }

    function delete_client(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/worker/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'The Worker  has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_client'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);
                    }
                });
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This  Worker is safe',
                'error'
                )
            }
        })
    }
    </script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


