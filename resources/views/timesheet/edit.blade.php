@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/file_upload.css')}}" rel="stylesheet" type="text/css" />
<style>
    .num-mark{
        margin-right: 28px !important;
        margin-top: 25px !important;
    }
</style>
@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Timesheet </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Edit Timesheet</span>
    <a href="{{url('/timesheet')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        Timesheet list
        </a>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch" required>
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid " id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            New Change Order
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="create_project" action="{{url('/timesheet/update/')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}"/>
                    <input type="hidden" name="timesheet_id" value="{{$timesheet->id}}"/>
                    <div class="kt-portlet__body ">
                        <div class="form-group row divide pb-4">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="main-title">Project:</label>
                                   <div class="input-group">
                                        <select class="form-control kt-select2" name="project_id" id="project_id" required>
                                            <option></option>
                                            @foreach($projects as $project)
                                                <option data-option="{{$project->ticket_option}}" value="{{$project->id}}" @if ($project->id == $timesheet->project_id)
                                                    selected
                                                @endif  >{{$project->project_name}}</option>
                                            @endforeach
                                        </select>
                                   </div>
                                    <span class="form-text text-muted">Please select a Project Name</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">Work Date:</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="work_date" readonly placeholder="Enter optional end date" id="kt_datepicker_2" value="{{{$timesheet->work_date}}}" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please enter Date</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row divide pb-4">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">Start Time:</label>

                                        <div class="input-group timepicker">
                                            <input class="form-control" name="start_time" id="kt_timepicker_1" readonly placeholder="Select time" type="text" value="{{{$timesheet->start_time}}}" required/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>

                                    <span class="form-text text-muted">Please enter start time</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group ">
                                    <label class="main-title">End Time:</label>

                                        <div class="input-group timepicker">
                                            <input class="form-control" name="end_time" id="kt_timepicker_2" readonly placeholder="Select time" type="text" value="{{{$timesheet->end_time}}}" required/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>

                                    <span class="form-text text-muted">Please enter end time</span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row divide ">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="main-title">Add Break:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
                                            <label>
                                                <input type="checkbox" id="add_break" @if ($timesheet->break == 1)
                                                    checked
                                                @endif name="">
                                                <span></span>
                                            </label>
                                            <input type="hidden" name="break" id="break" value="{{{$timesheet->break}}}" >
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row divide ">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label class="main-title">Add Overtime:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info">
                                            <label>
                                                <input type="checkbox" id="add_overtime" name=""  @if ($timesheet->overtime == 1)
                                                    checked
                                                @endif>
                                                <span></span>
                                            </label>
                                            <input type="hidden" name="overtime" id="overtime" value="{{{$timesheet->overtime}}}">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row  " >
                            <div class="col-md-6" id="labor_section">
                                <label class="main-title">Workers:</label>
                                @foreach ($timesheet->worker as $key => $worker_one)


                                <div class="form-group row  labor_group">
                                    <div class="col-md-1 color-stick text-right">
                                        <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >{{$key+1}}</span>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="sub-title">worker</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2" name="worker[]" id="worker" required>

                                                @foreach($workers as $worker)
                                                    <option  value="{{$worker->id}}" @if ($worker_one == $worker->id)
                                                        selected
                                                    @endif>{{$worker->worker_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <label class="sub-title">Hours</label>
                                        <div class="input-group date">
                                            <input type="number" class="form-control labor_hours" name="hours[]"   placeholder="Hours (per person)" value="{{$timesheet->hours[$key]}}"  required/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    hrs
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div  class="col-md-6"></div>
                            <div class="col-md-6 pb-4 text-right">
                                <button type="button" id="add_labor" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Labor</button>
                                <button type="button" id="remove_labor" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Labor</button>
                            </div>
                        </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="reset" class="btn btn-primary" id="submit-project">Update</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
</div>

<!-- end:: Content -->
@endsection

@section('page-js')

    <script>
        "user strict";
        var create_project = function(){

            var create_p= $('#create_project');

            var submit_new_project = function(){
                $('#submit-project').click(function(e){
                    console.log('sdfsd');
                    e.preventDefault();
                    var btn = $(this);
                    var form = btn.closest('form');
                    form.validate({
                        rules: {
                            project_name: {
                                required:true,

                            },
                            work_date:{
                                required:true,
                            },
                            start_time:{
                                required:true,
                            },
                            end__time:{
                                required: true,
                            },
                            break:{
                                required: true,
                                number:true,
                            },
                            overtime:{
                                required: true,
                                number:true,
                            },
                            breakdown:{
                                required: true,
                            },
                            worker:{
                                required: true,

                            },
                            hours:{
                                required: true,
                                number:true,
                            }
                        }
                    })

                    if (!form.valid()) {
                        return;
                    }

                    form.submit();


                });
            }

            //for timesheet

            var time_picker = function(){
                $('#kt_timepicker_2, #kt_timepicker_1').timepicker({
                    minuteStep: 1,
                    defaultTime: '',
                    showSeconds: true,
                    showMeridian: false,
                    snapToStep: true
                });

            }

            var add_break = function(){
                $('#add_break').click(function(){
                    _this = $('#break').val();
                    if(_this == 1){
                        $('#break').val(0);
                    }   else {
                        $('#break').val(1);
                    }

                });
            }

            var add_overtime = function(){
                $('#add_overtime').click(function(){
                    _this = $('#overtime').val();
                    if(_this == 1){
                        $('#overtime').val(0);
                    }   else {
                        $('#overtime').val(1);
                    }

                });
            }

            return {
                init: function(){
                    add_break();
                    add_overtime();
                    time_picker();
                    submit_new_project();


                }
            };


        }();

        jQuery(document).ready(function(){
            create_project.init();
        });

        $('#project_id').select2({
            placeholder: "Select an Project Name",
            minimumResultsForSearch: Infinity
        });


    $(function(){
        $(document).on('click','#add_labor',function(e){

        e.preventDefault();
        var repeater_group = $('#labor_section'),
        current_item = $(".labor_group").last(),
        number = current_item.find('.num-mark').text()/1+1,
        new_item = (current_item.clone()).appendTo(repeater_group);
        new_item.find('input').val('');
        //console.log(new_item.find('.num-mark').first().html())
        new_item.find('.num-mark').first().html(number);
        //$(this).closest('.repeater-item').find('.add-item').remove();
        //new_item.find('.add-item').remove();
        }).on('click','#remove_labor',function(e){
        if($('.labor_group').length != 1){
            e.preventDefault();
            $('.labor_group').last().remove();

            }
            return false;

        })

    })

    // $('#worker').select2({
    //     placeholder: "Select a worker",
    //     minimumResultsForSearch: Infinity
    // });

    </script>


    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


