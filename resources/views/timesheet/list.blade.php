@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Timesheet </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Timesheet List</span>
    {{-- <a href="{{url('/create_project')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New Project
        </a> --}}
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">

        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/timesheet/create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Timesheet
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">

                </div>
                <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                    <a href="#" class="btn btn-default kt-hidden">
                        <i class="la la-cart-plus"></i> New Order
                    </a>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--end: Search Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">

       <div class='row'>
            <div class='col-md-12'>
                 <!--begin: Datatable -->

                <table class="table" id="example" width="100%">
                    <thead>
                        <tr>
                            <th title="Field #1">Created Time</th>
                            <th title="Field #2">Project Name</th>
                            <th title="Field #3">Work Date</th>
                            <th title="Field #4">Start Time</th>
                            <th title="Field #5">End Time</th>
                            <th title="Field #6">Workers</th>
                            <th title="Field #7">manager</th>
                            <th title="Field #8">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($timesheets))
                            @foreach($timesheets as $row)
                                <tr>
                                    <td data-date="{{$row->created_at}}">{{$row->created_at}}</td>
                                    <td>{{$row->project->project_name}}</td>
                                    <td>{{$row->work_date}}</td>
                                    <td>{{$row->end_time}}</td>
                                    <td>{{$row->start_time}}</td>
                                    <td>{{count($row->worker)}}</td>
                                    <td>{{$row->user->name}}</td>
                                    <td>
                                        <a href="{{url('/timesheet/'.$row->id.'/edit')}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a onclick="deleted_record({{$row->id}})" id="{{'del'.$row->id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
                                            <i class="fa fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
       </div>
    </div>
</div>
<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')


    <script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/datatables.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/timesheet_list.js')}}" type="text/javascript"></script>


    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


