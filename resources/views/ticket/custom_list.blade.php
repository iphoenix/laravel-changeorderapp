@extends('layout.master')



@section('custom-css')

<style>
    .fileupload-group{
    width: 100%;
    height: 100%;
    border: 4px dashed #fff;
    }
    .fileupload-group p{
        width: 100%;
    height: 100%;
    text-align: center;
    line-height: 47px;
    color: #581c1c;
    font-family: Arial;
    font-size: 23px;
    }
    .fileupload-group input{
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
    }
    #new_client{
        top:25%;
    }
    .main-title{
        font-size: 20px !important;
        font-weight: 600 !important;
    }
    .file-area {
    width: 100%;
    position: relative;
    border: 1px solid #8e1515;
        box-shadow: 2px 2px 9px 0px;
    }
    .file-area input[type=file] {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0;
    cursor: pointer;
    }
    .file-area .file-dummy {
    width: 100%;
    padding: 30px;
    background: rgba(255, 255, 255, 0.2);
    border: 2px dashed rgba(255, 255, 255, 0.2);
    text-align: center;
    transition: background 0.3s ease-in-out;
    }
    .file-area .file-dummy .success {
    display: none;
    }
    .file-area:hover .file-dummy {
    background: rgba(255, 255, 255, 0.1);
    }
    .file-area input[type=file]:focus + .file-dummy {
    outline: 2px solid rgba(255, 255, 255, 0.5);
    outline: -webkit-focus-ring-color auto 5px;
    }
    .file-area input[type=file]:valid + .file-dummy {
    border-color: rgba(0, 255, 0, 0.4);
    background-color: rgba(0, 255, 0, 0.3);
    }
    .file-area input[type=file]:valid + .file-dummy .success {
    display: inline-block;
    }
    .file-area input[type=file]:valid + .file-dummy .default {
    display: none;
    }
    .kt-datatable__toggle-detail{
        display: hidden;
    }
</style>

@endsection

@section('custom-js')
{{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Ticket </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Ticket List</span>

    </div>

</div>
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">

        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/new_ticket')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Ticket
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">

        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="row">
                <div class=" col-md-12">
                    <table  id="custom_table" width="100%" class="table border-table display">
                        <thead>
                            <tr>
                                <th title="Field #1" >Ticket</th>
                                <th title="Field #3">Work Date</th>
                                <th title="Field #4">Submitted</th>
                                <th title="Field #5">Status</th>
                                <th title="Field #6" class="display_status" style="width:20%;">Attachment</th>
                            </tr>
                        </thead>
                        <tbody >

                                @foreach($tickets as $ticket)
                                <tr>

                                    <td class="kt-datatable__cell" data-project="{{$ticket->project_name}}"><a href="{{url('/ticketview/').'/'.$ticket->id}}"> {{$ticket->ticket_num}} - {{$ticket->ticket_subject}}</a></td>
                                    {{-- <td class="kt-datatable__cell">{{$ticket->ticket_subject}}</td> --}}
                                    <td class="kt-datatable__cell">{{$ticket->start_date}}</td>
                                    <td class="kt-datatable__cell" data-year = "{{date('Y',strtotime($ticket->created_at))}}">{{date('m/d/Y',strtotime($ticket->created_at))}}</td>
                                    <td class="kt-datatable__cell">
                                        @if($ticket->t_status == 0)
                                            <span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill'>Draft</span>
                                        @elseif($ticket->t_status == 1)
                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>Submitted</span>
                                        @elseif($ticket->t_status == 2)
                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>revise</span>
                                        @elseif($ticket->t_status == 3)
                                        <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Approved</span>
                                        @elseif($ticket->t_status == 4)
                                        <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Closed</span>
                                        @endif
                                    </td>
                                    <td class="attachment display_status">
                                        @php
                                            //var_dump(json_decode($ticket->original_photoname));
                                            if(!empty(json_decode($ticket->photoname))){
                                                //var_dump(json_decode($ticket->photoname));
                                                $index = 1;
                                                foreach(json_decode($ticket->photoname) as $photoname1){
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'"." onClick=showphoto('".asset("storage/app/public/photos/".$photoname1)."')". ">".$index."</span>";
                                                $index++;
                                                }
                                            }
                                        @endphp
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Ticket</th>
                                <th>Wrok Date</th>
                                <th>Submitted</th>
                                <th>Status</th>
                                <th class="display_status">Attachment</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>


            <!--end: Datatable -->
        </div>

        <!--end: Search Form -->
    </div>

</div>
<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>



  

    <script>


    $('#active_list').select2({
        placeholder: "Active Projects   ({{$projects->count()}})",
        minimumResultsForSearch: Infinity
    });

    $('#select_project').select2({
        placeholder: "Select project",
        minimumResultsForSearch: Infinity
    });

    $('#select_year').select2({
        placeholder: "Select year",
        minimumResultsForSearch: Infinity,

    });

    $(function(){
        $('#select_project').on('select2:select', function(e){
            var data = e.params.data;
            var search =  $('tfoot').find('th').eq('0').children();
            var year = $('#select2-select_year-container').text();

            filter_table(data.text, year);
        });

        $('#select_year').on('select2:select', function(e){
            var data = e.params.data;
            var search =  $('tfoot').find('th').eq('0').children();
            var project = $('#select2-select_project-container').text();
            filter_table(project,data.text);

        });
    })

    function showphoto(url){

        swal.fire({
            title: 'Attached image!',
            html: "<a  href='"+url+"' target='_blank' >Show full image.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageUrl: url,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            animation: true
        });

    }


   // Class definition

    var KTBootstrapSwitch = function() {

        // Private functions
        var demos = function() {
        // minimum setup
            $('[data-switch=true]').bootstrapSwitch();
        };

        $('#attachment_show').on("switchChange.bootstrapSwitch", function (event, state) {

            console.log(state); // true | false
            if(state == true)
                $(".display_status").css('display','table-cell');
            else
                $(".display_status").css('display','none');
        });

        return {

        init: function() {
            demos();
        },
        };
    }();

    jQuery(document).ready(function() {
    KTBootstrapSwitch.init();
    });
    </script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


