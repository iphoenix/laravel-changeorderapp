@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/ticket_new.css')}}" rel="stylesheet" type="text/css" />


@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Ticket </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">New Ticket</span>
    {{-- <a href="{{url('/new_ticket')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New
        </a> --}}
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch" required>
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            New Ticket
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="create_project" action="{{url('new_ticket')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group row divide">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="main-title">Project:</label>
                                   <div class="input-group">
                                        <select class="form-control kt-select2" name="project_id" id="project_id">
                                            <option></option>
                                            @foreach($projects as $project)
                                                <option data-option="{{$project->ticket_option}}" value="{{$project->id}}">{{$project->project_name}}</option>
                                            @endforeach
                                        </select>
                                   </div>
                                    <span class="form-text text-muted">Please select a Project Name</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="main-title">Subject:</label>
                                <input type="text" class="form-control" name="ticket_subject" placeholder="Enter project name" required>
                                <span class="form-text text-muted">Please enter Subject</span>
                            </div>
                        </div>
                      

                        <div class="form-group row divide">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="main-title">Ticket Number:</label>
                                <input type="text" class="form-control" name="ticket_num" placeholder="Enter ticket number" required>
                                <span class="form-text text-muted">Please enter Ticket Number</span>
                            </div>
                         
                            <div class="col-md-12">
                                <br>
                            </div>
                        </div>
                        <div class="form-group row divide">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group ">
                                    <label class="main-title">Work Start Date:</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="start_date" readonly placeholder="Enter work start date" id="kt_datepicker_2" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <span class="form-text text-muted">Please enter Work Start Date</span>

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group ">
                                    <label class="main-title">Optional End Date:</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="end_date" readonly placeholder="Enter optional end date" id="kt_datepicker_2" required/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <span class="form-text text-muted">Please enter Optional End Date</span>

                                </div>
                            </div>

                        </div>
                        <div class="form-group row divide ">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-gropu">
                                    <label class="main-title">Work Description:</label>
                                    <textarea class="form-control" name="work_description" id="address" rows="3" style="overflow: auto; overflow-wrap: break-word; resize: none; height: 130px;" required></textarea>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label class="main-title">Add photos</label>

                                <div class="form-group file-area fileupload-group" >
                                    <input type="file"  name="photos[]" multiple required>
                                    <p>Drag your photo here (jpg, jpeg, png, gif, svg).</p>
                                </div>
                            </div>

                        </div>
                        <div id='advanced_form'>
                            {{-- labor breakdown --}}
                            <div class="form-group row " >
                                <div class="col-md-12" id="labor_section">
                                    <label class="main-title">Labor Breakdown</label>
                                    <div class="form-group row  labor_group">
                                        <div class="col-md-1 color-stick text-right">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                        </div>
                                        <div class="col-md-2 kt-timeline-v3__item kt-timeline-v3__item--info">
                                            <label class="sub-title">Labor Type</label>
                                                <select class="form-control"  name="labor_type[]">

                                                    @foreach($labors as $labor)
                                                        <option value="{{$labor->name }}">{{$labor->name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="sub-title">Rate Type</label>

                                                <select class="form-control labor_rate" name="labor_rate_type[]" >

                                                    @foreach($rates as $rate)
                                                        <option value="{{$rate->name}}">{{$rate->name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="sub-title">Headcount</label>
                                            <input type="text" class="form-control " name="labor_headcount[]" placeholder="Headcount" >

                                        </div>
                                        <div class="col-md-2">
                                            <label class="sub-title">Hours (per person)</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control labor_hours" name="labor_hours[]"  placeholder="Hours (per person)"  />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        hrs
                                                    </span>
                                                </div>
                                            </div>



                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Rate (per hour)</label>
                                            <div class="input-group ">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        $
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control labor_rate" name="labor_rate[]"  placeholder="Rate (per hour)"  />

                                            </div>


                                        </div>


                                    </div>

                                </div>
                                <div class="col-md-12 pt-1 text-right">
                                    <button type="button" id="add_labor" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Labor</button>
                                    <button type="button" id="remove_labor" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Labor</button>
                                </div>
                            </div>
                            {{-- labor breakdown description --}}
                            <div class="form-group row ">
                                <div class="col-md-1"></div>
                                <div class="col-md-11" id="labor_des_section">

                                    <div class="form-group row labor_des_group">

                                        <div class="col-md-1 color-stick1 text-right">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                        </div>
                                        <div class="col-md-8 ">
                                            <label class="sub-title">Description</label>
                                            <input type="text" class="form-control labor-description" name="labor_description[]" placeholder="Description"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Amount</label>
                                            <div class="input-group ">
                                                <input type="text" class="form-control labor-amount" name="labor_amount[]"  value="0"   />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        %
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 pt-1 text-right">
                                    <button type="button" id="add_labor_des" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Makeup</button>
                                    <button type="button" id="remove_labor_des" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Makeup</button>
                                </div>
                            </div>
                            {{-- Material Breakdown --}}
                            <div class="form-group row divide-up">
                                <div class="col-md-12" id="material_section">
                                    <label class="main-title">Material Breakdown</label>
                                    <div class="form-group row material_group">
                                        <div class="col-md-1 color-stick text-right">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                        </div>
                                        <div class="col-md-2 kt-timeline-v3__item kt-timeline-v3__item--info">
                                            <label class="sub-title">Material Type</label>
                                            <input type="text" class="form-control material_type" name="material_type[]" placeholder="Material Type"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Quantity</label>
                                            <input type="text" class="form-control material_quentity" name="material_quentity[]" placeholder="Quantity"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Unit of Measure</label>
                                            <input type="text" class="form-control material_unit" name="material_unit[]" placeholder="Unit of Measure"  >

                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Hours (per unit)</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control material_hours" name="material_hours[]"  placeholder="Hours (per unit)"  />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        hrs
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 pt-1 text-right">
                                    <button type="button" id="add_material" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Material</button>
                                    <button type="button" id="remove_material" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Material</button>
                                </div>
                            </div>
                            {{-- material breakdown description --}}
                            <div class="form-group row ">
                                <div class="col-md-1"></div>
                                <div class="col-md-11" id="material_des_section">

                                    <div class="form-group row material_des_group">

                                        <div class="col-md-1 color-stick1 text-right">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                        </div>
                                        <div class="col-md-8 ">
                                            <label class="sub-title">Description</label>
                                            <input type="text" class="form-control material_description" name="material_description[]" placeholder="Description"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Amount</label>
                                            <div class="input-group ">
                                                <input type="text" class="form-control material_amount" name="material_amount[]"  value="0"   />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        %
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 pt-1 text-right">
                                    <button type="button" id="add_material_des" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Makeup</button>
                                    <button type="button" id="remove_material_des" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Makeup</button>
                                </div>
                            </div>
                            {{-- Equipment Breakdown --}}
                            <div class="form-group row divide">
                                <div class="col-md-12 " id="equip_section">
                                    <label class="main-title">Equipment Breakdown</label>
                                    <div class="form-group row  equip_group">
                                        <div class="col-md-1 color-stick text-right">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                        </div>
                                        <div class="col-md-2 kt-timeline-v3__item kt-timeline-v3__item--info">
                                            <label class="sub-title">Equipment Type</label>
                                            <input type="text" class="form-control equip_type" name="equip_type[]" placeholder="Equipment Type"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Quantity</label>
                                            <input type="text" class="form-control equip_quantity" name="equip_quantity[]" placeholder="Quantity"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Unit of Measure</label>
                                            <input type="text" class="form-control equip_unit" name="equip_unit[]" placeholder="Unite of Measure"  >

                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Hours (per unit)</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control equip_hours" name="equip_hours[]"  placeholder="Hours (per unit)"  />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        hrs
                                                    </span>
                                                </div>
                                            </div>



                                        </div>



                                    </div>
                                </div>
                                <div class="col-md-12 pt-1 text-right">
                                    <button type="button" id="add_equip" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Equipment</button>
                                    <button type="button" id="remove_equip" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Equipment</button>
                                </div>
                            </div>
                            {{-- Equipment breakdown description --}}
                            <div class="form-group row ">
                                <div class="col-md-1"></div>
                                <div class="col-md-11" id="equip_des_section">

                                    <div class="form-group row equip_des_group ">

                                        <div class="col-md-1 color-stick1 text-right">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bolder num-mark" >1</span>
                                        </div>
                                        <div class="col-md-8 ">
                                            <label class="sub-title">Description</label>
                                            <input type="text" class="form-control equip_description" name="equip_description[]" placeholder="Description"  >
                                        </div>
                                        <div class="col-md-3">
                                            <label class="sub-title">Amount</label>
                                            <div class="input-group ">
                                                <input type="text" class="form-control " name="equip_amount[]"  value="0"  />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        %
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 pt-1 text-right">
                                    <button type="button" id="add_equipment_des" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add Makeup</button>
                                    <button type="button" id="remove_equipment_des" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i> Remove Makeup</button>
                                </div>
                            </div>
                        </div>




                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="reset" class="btn btn-primary" id="submit-project">Save</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Client Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name" >
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new client</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')


    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/ticket_new.js')}}" type="text/javascript"></script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


