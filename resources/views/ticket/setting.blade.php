@extends('layout.master')

<style>
    .attachment{
        cursor: pointer;
    }
    #new_client{
        top:15% !important;
    }
    #new_company{
        top:15% !important;
    }
    #labor_update_form{
        top:15% !important;
    }
</style>
@section('custom-css')

@endsection
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Ticket </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Ticket Setting</span>

    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            {{-- <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span> --}}
            {{-- <h3 class="kt-portlet__head-title">
                HTML Table
                <small>Datatable initialized from HTML table</small>
            </h3> --}}
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/ticket/new')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Ticket
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div class="kt-portlet__body kt-portlet__body--fit">
        <div class="row">
            <div class="col-md-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head row">
                        <div class="col-md-6 text-left mt-3">
                            <h2><i class="flaticon2-settings"></i>&nbsp;&nbsp;&nbsp;Labor type</h2>
                        </div>
                        <div class="col-md-6 text-right mt-3">
                            <button type="button" data-toggle='modal' data-target="#new_client" id="add_client" class="btn btn-primary btn-sm btn-elevate  btn-icon"><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                        <div class="kt-section">

                            <div class="kt-section__content">
                                <table class="table" id="client" width="100%">
                                    <thead>
                                        <tr>
                                            <th title="Field #1">Labor name</th>
                                            <th title="Field #2">Created time</th>
                                            <th title="Field #3" style="width:30px !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($labors))
                                            @foreach($labors as $labor)
                                                <tr>
                                                    <td>{{$labor->name}}</td>
                                                    <td>{{$labor->created_at}}</td>
                                                    <td>
                                                        <a onclick="delete_labor({{$labor->labor_id}})" id="{{'delete_labor'.$labor->labor_id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
                                                        <i class="fa fa-trash-alt"></i>
                                                        </a>

                                                        <a onclick="update_labor({{$labor->labor_id}})" class="btn btn-sm btn-clean btn-icon btn-icon-md delete_record" title="Delete">
                                                        <i class="fa fa-edit"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head row">
                        <div class="col-md-6 text-left mt-3">
                            <h2><i class="flaticon2-settings"></i>&nbsp;&nbsp;&nbsp;Rate type</h2>
                        </div>
                        <div class="col-md-6 text-right mt-3">
                            <button type="button" id="add_company" data-toggle='modal' data-target="#new_company" class="btn btn-primary btn-sm btn-elevate  btn-icon"><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Section-->
                        <div class="kt-section">

                            <div class="kt-section__content">
                                <table class="table" id="company" width="100%">
                                    <thead>
                                        <tr>

                                            <th title="Field #1">Rate type</th>
                                            <th title="Field #5">Created time</th>
                                            <th title="Field #6" style="width:30px !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($rates))
                                            @foreach($rates as $rate)
                                                <tr>
                                                <td>{{$rate->name}}</td>
                                                <td>{{$rate->created_at}}</td>
                                                <td>
                                                    <a onclick="delete_rate({{$rate->rate_id}})" id="{{'delete_rate'.$rate->rate_id}}" class="btn btn-sm btn-clean btn-icon btn-icon-md " title="Delete">
                                                    <i class="fa fa-trash-alt"></i>
                                                    </a>
                                                    <a onclick="update_rate({{$rate->rate_id}})" class="btn btn-sm btn-clean btn-icon btn-icon-md " >
                                                        <i class="fa fa-edit"></i>
                                                        </a>

                                                </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Labor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new labor</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!--begin::new_company Modal-->
<div class="modal fade" id="new_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Create New Rate</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label"> Name:</label>
                        <input type="text" name='name' class="form-control" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new_company_btn" class="btn btn-primary">Create new Rate</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::update labor Modal-->
<div class="modal fade" id="labor_update_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Update labor name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <input type="hidden" id="labor_id" name="labor_id" value="">
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label"> Name:</label>
                        <input type="text" name='name' id="labor_name" class="form-control text-right" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="labor_update_btn" class="btn btn-primary">Update labor name</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end:update labor Modal-->

<!--begin::update rate Modal-->
<div class="modal fade" id="rate_update_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Update rate name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <input type="hidden" id="rate_id" name="rate_id" value="">
                    <div class="form-group">
                        <label for="new-client-name"  class="form-control-label"> Name:</label>
                        <input type="text" name='name' id="rate_name" class="form-control text-right" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="rate_update_btn" class="btn btn-primary">Update rate name</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<!--end:update rate Modal-->


<!-- end:: Content -->
@endsection

@section('page-js')


    <script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/custom-js/datatables.bootstrap4.min.js')}}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $("#client").DataTable({
                aaSorting: [],
                responsive: true,

                columnDefs: [
                {
                    responsivePriority: 1,
                    targets: 0
                },
                {
                    responsivePriority: 2,
                    targets: -1
                }
                ]
            });

            $("#company").DataTable({
                aaSorting: [],
                responsive: true,

                columnDefs: [
                {
                    responsivePriority: 1,
                    targets: 0
                },
                {
                    responsivePriority: 2,
                    targets: -1
                }
                ]
            });

            $(".dataTables_filter input")
                .attr("placeholder", "Search here...")
                .css({
                width: "100px",
                display: "inline-block"
                });

            $('[data-toggle="tooltip"]').tooltip();
        });

        "use strict";
    // Class definition

    var KTDatatableHtmlTableDemo = function() {


        var new_client = function()
                {
                    $('#new-client-btn').click(function(){
                        var _this = $(this);
                        var client_modal = $('#new_client');
                        var form = _this.closest('form');
                        var inputs = $('#new_client input');
                        form.validate({
                            rules:{
                                new_client_name:{
                                    required:true,
                                },
                            }
                        })

                        if(!form.valid()){
                            return;
                        }
                        _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                        form.ajaxSubmit({
                        url: "{{url('/labor/create')}}",
                        success: function(response, status, xhr, $form) {
                            // similate 2s delay
                            if(response.success == 'true'){
                                swal.fire("New Labor has been created!", "Please use new Labor.","success");
                                _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                client_modal.modal('hide');
                                form[0].reset();
                                setTimeout(function(){ location.reload(true); }, 1000);



                                //location.reload();
                            } else {
                                swal.fire("The Labor existed with same name!","Please use another name.", "error");
                                _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                client_modal.modal('hide');
                                form[0].reset();

                                // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                            }
                        },
                        error: function() {

                                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                                showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                        }
                    });


                    })

                }
        var new_company = function()
        {
            $('#new_company_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#new_company');
                var form = _this.closest('form');
                var inputs = $('#new_client input');
                form.validate({
                    rules:{
                        new_client_name:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/rate/create')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success == 'true'){
                        swal.fire("New Rate has been created!", "Please use new Rate.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The Rate existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }

        var stop_enter = function (){
            $('form input').keydown(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();

                    swal.fire("Please click submit button!","- Do not use enter key-.", "error");
                    return false;
                }
            });
        }

        var update_labor = function()
        {
            $('#labor_update_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#labor_update_form');
                var form = _this.closest('form');
                var inputs = $('input');
                var btn = $('#labor_update_btn');
                form.validate({
                    rules:{
                        name:{
                            required:true,
                        },
                        labor_id:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/labor/update')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success){
                        swal.fire("The labor name has been updated!", "Please make sure it.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The labor name existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }

        var update_rate = function()
        {
            $('#rate_update_btn').click(function(){
                var _this = $(this);
                var client_modal = $('#rate_update_form');
                var form = _this.closest('form');
                var inputs = $('input');
                var btn = $('#rate_update_btn');
                form.validate({
                    rules:{
                        name:{
                            required:true,
                        },
                        rate_id:{
                            required:true,
                        },
                    }
                })

                if(!form.valid()){
                    return;
                }
                _this.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                form.ajaxSubmit({
                url: "{{url('/rate/update')}}",
                success: function(response, status, xhr, $form) {
                    // similate 2s delay
                    if(response.success){
                        swal.fire("The rate name has been updated!", "Please make sure it.","success");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        setTimeout(function(){ location.reload(true); }, 1000);

                        //location.reload();
                    } else {
                        swal.fire("The rate name existed with same name!","Please use another name.", "error");
                        _this.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        client_modal.modal('hide');
                        form[0].reset();

                        // showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
                    }
                },
                error: function() {

                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');


                }
            });


            })

        }


        return {
            // Public functions
            init: function() {

                new_client();
                new_company();
                stop_enter();
                update_labor();
                update_rate();

            },
        };
    }();

    jQuery(document).ready(function() {
        KTDatatableHtmlTableDemo.init();
    });

    function update_labor(labor_id){
        $('#labor_id').val(labor_id);
        var id = '#delete_labor'+labor_id;
        $('#labor_name').val($(id).parent().prev().prev().text());
        $('#labor_update_form').modal('show');
    }

    function update_rate(rate_id){
        $('#rate_id').val(rate_id);
        var id = '#delete_rate'+rate_id;
        $('#rate_name').val($(id).parent().prev().prev().text());
        $('#rate_update_form').modal('show');
    }

    function delete_labor(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/labor/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'The Labor  has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_labor'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);
                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This  Rate is safe',
                'error'
                )
            }
        })

    }

    function delete_rate(record_id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/rate/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'the Rate has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_rate'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);

                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This Rate is safe',
                'error'
                )
            }
        })

    }

    function edit_company(record_id){




        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You want to delete this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}',
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/company/delete') }}",
                    method: 'post',
                    data: {
                        record_id: record_id,

                    },
                    success: function(result){
                        swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                        var del_id = '#delete_rate'+record_id;
                        $(del_id).closest('tr').remove();
                        setTimeout(function(){ location.reload(true); }, 1000);

                    }
                });


            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your  file is safe',
                'error'
                )
            }
        })

    }
    </script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


