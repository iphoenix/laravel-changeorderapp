@extends('layout.master')

@section('custom-css')
<link href="{{asset('public/custom-css/ticket_view.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Ticket </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">New Ticket</span>
    <a href="{{url('/new_ticket')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
            Add New
        </a>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch" required>
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>

</div>

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                            Ticket
                        </h2>
                    </div>
                </div>

                <!--begin::Form-->
                <div class="container-fuild pt-3 pl-3">
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content Head -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">
								<h3 class="kt-subheader__title">
									Ticket View
								</h3>
								<span class="kt-subheader__separator kt-subheader__separator--v"></span>
								<div class="kt-subheader__group" id="kt_subheader_search">
									<span class="kt-subheader__desc" id="kt_subheader_total">
                                        Submit, Update, Delete ticket.
										 </span>
								</div>
							</div>
							{{-- <div class="kt-subheader__toolbar">
								<a onclick="goBack()" class="btn btn-default btn-bold">
									Back </a>
							</div> --}}
						</div>

						<!-- end:: Content Head -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--Begin:: Portlet-->
							<div class="kt-portlet">
								<div class="kt-portlet__body">
									<div class="kt-widget kt-widget--user-profile-3">
										<div class="kt-widget__top">
											{{-- <div class="kt-widget__media">
												<img src="./assets/media/users/100_12.jpg" alt="image">
											</div> --}}
											<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-bolder kt-font-light kt-hidden">
												JM
											</div>
											<div class="kt-widget__content">
												<div class="kt-widget__head">
													<div class="kt-widget__user">
														<a  class="kt-widget__username">
                                                            <h2>Ticket &NonBreakingSpace;{{$ticket->ticket_num." - ". $ticket->ticket_subject}}</h2>
                                                        </a>

                                                    </div>

                                                </div>
                                                <div class="kt-widget__head">
													<div class="kt-widget__user">
														<a  class="kt-widget__username">
                                                            @if($ticket->t_status == 0)
                                                                <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" >  draft</span></h4>
                                                            @elseif($ticket->t_status == 1)
                                                            <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > submited</span></h4>
                                                            @elseif($ticket->t_status == 2)
                                                            <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > revise</span></h4>
                                                            @elseif($ticket->t_status == 3)
                                                            <h4>Status:  &NonBreakingSpace; <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" >approved</span></h4>
                                                            @elseif($ticket->t_status == 4)
                                                            <h4>Status:  &NonBreakingSpace;<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" > closed</span></h4>

                                                            @endif
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__head">
													<div class="kt-widget__user">
														<a  class="kt-widget__username">
                                                            <h4> Latest Activity:  &NonBreakingSpace;
                                                                @if($latest_activity->history_status == 0)
                                                                    Item created
                                                                @elseif($latest_activity->history_status == 1)
                                                                    Item submitted
                                                                @elseif($latest_activity->history_status == 2)
                                                                Item revised
                                                                @elseif($latest_activity->history_status == 3)
                                                                Item approved
                                                                @elseif($latest_activity->history_status == 4)
                                                                Item closed
                                                                @elseif($latest_activity->history_status == 5)
                                                                Item updated
                                                                @endif
                                                            </h4>
                                                        </a>
                                                    </div>
												</div>
												<div class="kt-widget__subhead">
													<a ><i class="flaticon2-new-email"></i>{{$latest_activity->get_user->name}}</a>
                                                    <a ><i class="flaticon-calendar-with-a-clock-time-tools"></i>
                                                        @if($latest_activity->created == 1)
                                                            {{date('F j, Y, g:i a',strtotime($latest_activity->created_at))}}
                                                        @else
                                                        {{date('F j, Y, g:i a',strtotime($latest_activity->updated_at))}}
                                                        @endif
                                                </a>
												</div>

											</div>
										</div>

									</div>
								</div>
							</div>

							<!--End:: Portlet-->
							<div class="row">
								<div class="col-xl-8 col-sm-12">
									<!--Begin:: Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h2 class="kt-portlet__head-title" style="font-size:2rem;">
													Ticket Detail
												</h2>
											</div>
											<div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-clean btn-sm btn-bold" data-toggle="dropdown">
													<i class="flaticon2-gear"></i>Status Setting
												</a>
												<div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">

													<!--begin::Nav-->
													<ul class="kt-nav">
														<li class="kt-nav__head">
															Status Settings
															<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
														</li>
														<li class="kt-nav__separator"></li>
														<li class="kt-nav__item">
															<a href="{{url('/ticket/'.$ticket->id.'/submit')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-new-email"></i>
																<span class="kt-nav__link-text">Submit</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="{{url('/ticket/'.$ticket->id.'/revise')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-attention"></i>
																<span class="kt-nav__link-text">Revised</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="{{url('/ticket/'.$ticket->id.'/approve')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-rocket-2"></i>
																<span class="kt-nav__link-text">Approved</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="{{url('/ticket/'.$ticket->id.'/close')}}" class="kt-nav__link">
																<i class="kt-nav__link-icon fa fa-door-closed"></i>
																<span class="kt-nav__link-text">Closed</span>
															</a>
														</li>
														<li class="kt-nav__separator"></li>

													</ul>

													<!--end::Nav-->
												</div>
                                            </div>
										</div>
										<div class="kt-form kt-form--label-right">
											<div class="kt-portlet__body">
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Project:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{$ticket->project_name}}</h4></span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Project #:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{$ticket->project_num}}</h4></span>
													</div>
                                                </div>
                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Ticket subject:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$ticket->ticket_subject}}</h4></span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Ticket #:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext"><span class="kt-font-bolder"><h4>{{$ticket->ticket_num}}</h4></span>
													</div>
                                                </div>
                                               
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Submitted Date:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder"><h4>{{date('m/d/Y',strtotime($ticket->created_at))}}</h4></span>
													</div>
												</div>
												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Work Date:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder">
															<span href="#"><h4>{{$ticket->start_date." - ".$ticket->end_date}}</h4></span>
														</span>
													</div>
                                                </div>

												<div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>Work Description:</h4></label>
													<div class="col-8">
														<span class="form-control-plaintext kt-font-bolder">
															<span href="#"><h4 style=" white-space: pre-line; ">{{$ticket->work_description}}</h4></span>
														</span>
													</div>
                                                </div>

                                                <div class="form-group form-group-xs row">
													<label class="col-4 col-form-label"><h4>photos:</h4></label>
													<div class="col-8">
                                                        <span class="form-control-plaintext kt-font-bolder">
                                                        @php

                                                        if(!empty(json_decode($ticket->photoname))){

                                                            //var_dump(json_decode($ticket->photoname));
                                                            $index = 1;
                                                            $original_name = json_decode($ticket->original_photoname);

                                                            foreach(json_decode($ticket->photoname) as $key=> $photoname1){

                                                            echo "<span class='attached-file kt-badge mb-1 kt-badge--success kt-badge--inline kt-badge--pill '"." onClick=showphoto('".asset("storage/app/public/photos/".$photoname1)."')". ">".$original_name[$key]."</span> &nbsp;&nbsp;";
                                                            $index++;
                                                            }

                                                        }
                                                        @endphp
                                                        </span>
														{{-- <span class="form-control-plaintext kt-font-bolder">
															<span href="#"><h4>{{$ticket->work_description}}</h4></span>
														</span> --}}
													</div>
												</div>

											</div>

                                        </div>
                                        <div class="kt-portlet__foot">
                                            <div class="kt-form__actions kt-space-between" style="text-align:right;display:block;">

                                                <a href="{{url('/ticket/'.$ticket->id.'/edit')}}" class="btn btn-label-brand btn-sm btn-bold">Edit</a>
                                                <a onclick="deleteTicket()" class="btn btn-label-brand btn-sm btn-bold">Delete</a>
                                                |
                                                <a href="{{url('/ticket/'.$ticket->id.'/print')}}" class="btn btn-label-brand btn-sm btn-bold"><i class="fa fa-print"></i></a>
                                            </div>
                                        </div>
									</div>

									<!--End:: Portlet-->
								</div>
								<div class="col-xl-4 col-sm-12">

									<!--Begin:: Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h2 class="kt-portlet__head-title" style="font-size:2rem;">
                                                    history
                                                </h2>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">

                                            <a href="{{url('/ticket/'.$ticket->id.'/history/clear')}}" class="btn btn-clean btn-sm btn-bold" >
                                                <i class="fa fa-trash-alt"></i> History clear
                                            </a>
                                            </div>

										</div>
										<div class="kt-portlet__body">
											<div class="tab-content ">
                                                 <!--begin::Preview-->
                                                    <div class="kt-demo">
                                                        <div class="kt-demo__preview">
                                                            <div class="kt-list-timeline">
                                                                <div class="kt-list-timeline__items">
                                                                    @if (!empty($ticket_history))
                                                                        @foreach($ticket_history as $his)

                                                                                <div class="kt-list-timeline__item">
                                                                                    <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>


                                                                                        @if($his->history_status == 0)
                                                                                        <span class="kt-list-timeline__icon flaticon2-position kt-font-danger"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->name}}
                                                                                        <span class="kt-badge kt-badge--danger kt-badge--inline">
                                                                                            Item created
                                                                                        @elseif($his->history_status == 1)
                                                                                        <span class="kt-list-timeline__icon flaticon2-new-email kt-font-success"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->name}}
                                                                                        <span class="kt-badge kt-badge--success kt-badge--inline">
                                                                                            Item submitted
                                                                                        @elseif($his->history_status == 2)
                                                                                        <span class="kt-list-timeline__icon flaticon2-note kt-font-danger"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->name}}
                                                                                        <span class="kt-badge kt-badge--danger kt-badge--inline">
                                                                                        Item revised
                                                                                        @elseif($his->history_status == 3)
                                                                                        <span class="kt-list-timeline__icon
                                                                                        flaticon2-rocket-2 kt-font-success"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->name}}
                                                                                        <span class="kt-badge kt-badge--success kt-badge--inline">
                                                                                        Item approved
                                                                                        @elseif($his->history_status == 4)
                                                                                        <span class="kt-list-timeline__icon flaticon2-attention  kt-font-primary"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->name}}
                                                                                        <span class="kt-badge kt-badge--primary kt-badge--inline">
                                                                                        Item closed
                                                                                        @elseif($his->history_status == 5)
                                                                                        <span class="kt-list-timeline__icon flaticon2-note kt-font-primary"></span>
                                                                                        <span class="kt-list-timeline__text">manager: {{$his->name}}
                                                                                        <span class="kt-badge kt-badge--primary kt-badge--inline">
                                                                                        Item updated
                                                                                        @endif
                                                                                    </span></span>
                                                                                    <span class="kt-list-timeline__time" style="width:145px;">{{date('F j, Y, g:i a', strtotime($his->created_at))}}</span>
                                                                                </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <!--end::Preview-->




											</div>
										</div>
									</div>

									<!--End:: Portlet-->
								</div>
							</div>
						</div>

						<!-- end:: Content -->
					</div>



                </div>


                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="new_client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="load_ctn" style="display: none;">
        <div class="m-loader m-loader--primary" style="width: 30px;display: inline-block;display: block;"></div>
    </div>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method='post'>

                @csrf
                    <div class="form-group">
                        <label for="new-client-name" name='new_client_name' class="form-control-label">New Client Name:</label>
                        <input type="text" name='new_client_name' class="form-control" id="new_client_name" required>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="new-client-btn" class="btn btn-primary">Create new client</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--end::Modal-->

<!-- end:: Content -->
{{-- Start : modal --}}
<form action="{{url('/ticket/'.$ticket->id.'/delete')}}" method="get" id="delete_form">
    @csrf
</form>
{{-- End : modal --}}
@endsection

@section('page-js')

    <!--begin::Page Vendors(used by this page) -->

    <script>
    function showphoto(url){

        swal.fire({
            title: 'Attached image!',
            html: "<a  href='"+url+"' target='_blank' >Show full image.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageUrl: url,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            animation: true
        });

    }

    function deleteTicket(){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "If you delete this project, related orders will be deleted.",
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $('#delete_form').submit();

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'This ticket is safe',
                'error'
                )
            }
        })

    }
    </script>
    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif

    @error('title')
    <script>
        swal.fire("{{ $message }}", "Please confirm it.","error");
    </script>
    @enderror




    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('public/assets/metronic/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/assets/metronic/js/demo1/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('public/assets/metronic/js/demo3/pages/dashboard.js')}}" type="text/javascript"></script> --}}

    <!--end::Page Scripts -->
@endsection


