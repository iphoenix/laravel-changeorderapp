@extends('layout.master')
@section('custom-css')

<link href="{{asset('public/custom-css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('public/custom-css/ticket-list.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('custom-js')
{{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}

@endsection

@section('main-content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Ticket </h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">Ticket List</span>

    </div>

</div>
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile p-4">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">

        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                    <a href="{{url('/ticket/new')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Ticket
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">

        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="row">
                <div class=" col-md-12">
                    <table  id="example" width="100%" class="table border-table display">
                        <thead>
                            <tr>
                                <th title="Field #1" >Ticket</th>
                                <th title="Field #3">Work Date</th>
                                <th title="Field #4">Submitted</th>
                                <th title="Field #5" style="width:100px;">Status</th>
                                <th title="Field #6">Project Name </th>
                                <th title="Field #7" style="width:100px;">Project Status</th>
                                <th title="Field #8" class="display_status" style="width:140px;">Attachment</th>
                                <td title='Field #9' style='width: 12px !important;'>Action</td>
                            </tr>
                        </thead>
                        <tbody >

                                @foreach($tickets as $ticket)
                                <tr>

                                    <td class="kt-datatable__cell" data-project="{{$ticket->project_name}}"><a > {{$ticket->ticket_num}} - {{$ticket->ticket_subject}}</a></td>
                                    {{-- <td class="kt-datatable__cell">{{$ticket->ticket_subject}}</td> --}}
                                    <td class="kt-datatable__cell">{{$ticket->start_date}}</td>
                                    <td class="kt-datatable__cell" data-year = "{{date('Y',strtotime($ticket->created_at))}}">{{date('F j, Y',strtotime($ticket->created_at))}}</td>
                                    <td class="kt-datatable__cell">
                                        @if($ticket->t_status == 0)
                                            <span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill'>Draft</span>
                                        @elseif($ticket->t_status == 1)
                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>Submitted</span>
                                        @elseif($ticket->t_status == 2)
                                        <span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>revise</span>
                                        @elseif($ticket->t_status == 3)
                                        <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Approved</span>
                                        @elseif($ticket->t_status == 4)
                                        <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Closed</span>
                                        @endif
                                    </td>
                                    <td>{{$ticket->project->project_name}}</td>
                                    <td>
                                        @if($ticket->project->p_status == 0)
                                            <span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill'>Draft</span>
                                        @else
                                            <span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Active</span>
                                        @endif

                                    </td>
                                    <td class="attachment display_status">
                                        @php
                                            //var_dump(json_decode($ticket->original_photoname));
                                            if(!empty(json_decode($ticket->photoname))){
                                                //var_dump(json_decode($ticket->photoname));
                                                $index = 1;
                                                foreach(json_decode($ticket->photoname) as $photoname1){
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'"." onClick=showphoto('".asset("storage/app/public/photos/".$photoname1)."')". ">".$index."</span>";
                                                $index++;
                                                }
                                            }
                                        @endphp
                                    </td>
                                    <td>
                                            <a href="{{url('/ticketview/').'/'.$ticket->id}}"><i class='fa fa-eye'></i></a>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th title="Field #1" >Ticket</th>
                                <th title="Field #3">Work Date</th>
                                <th title="Field #4">Submitted</th>
                                <th title="Field #5" style="width:100px;">Status</th>
                                <th title="Field #6">Project Name </th>
                                <th title="Field #7" style="width:100px;">Project Status</th>
                                <th title="Field #8" class="display_status" style="width:140px;">Attachment</th>
                                <td title='Field #9' style='width: 12px !important;'>Action</td>
                            
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--end: Datatable -->
        </div>

        <!--end: Search Form -->
    </div>

</div>
<!--end::Modal-->

<!-- end:: Content -->
@endsection

@section('page-js')




<script src="{{asset('public/custom-js/jquery.dataTables.min.js')}}" type="text/javascript"></script> 
<script src="{{asset('public/custom-js/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/uttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/custom-js/buttons.print.min.js')}}" type="text/javascript"></script>  

<script>



    function showphoto(url){

        swal.fire({
            title: 'Attached image!',
            html: "<a  href='"+url+"' target='_blank' >Show full image.</a> &nbsp;&nbsp;&nbsp;<a  href='"+url+"'  download>Download.</a>",
            imageUrl: url,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            animation: true
        });

    }


   // Class definition
   $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete: function () {
        this.api().columns([3,4,5]).every( function () {
            var column = this;
            var select = $('<select><option value="">Show all</option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
                var val = $('<div/>').html(d).text();
                select.append( '<option value="' + val + '">' + val + '</option>' );
                // select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
    }
    } );
} );


    </script>

    @if(session()->has('error'))
        <script>
            swal.fire("{{ session()->get('error') }}", "Please confirm it.","error");
        </script>
    @endif

    @if(session()->has('success'))
        <script>
            swal.fire("{{ session()->get('success') }}", "Please confirm it.","success");
        </script>
    @endif


@endsection


