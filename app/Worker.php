<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Worker extends Model
{
    //use SoftDeletes;
    public $table = 'workers';

    //



    public $fillable = [
        'worker_name',
    ];

    public $hidden = [
        'id',
    ];



    public $timestamps = true;


}
