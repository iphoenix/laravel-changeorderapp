<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    //use SoftDeletes;
    public $table = 'ticket';

    //



    public $fillable = [
        'id','project_id','ticket_subjet','photoname','ticket_num','bulletin_num','start_date','end_date','work_description','labor_collection','labor_des','material_collection','material_des','equipment_collection','equipment_des','photoname',
        'original_photoname', 'user_id'
    ];



    public $casts = [
        'labor_collection' =>'array',

    ];

    public $timestamps = true;

    public function project(){
        return $this->belongsTo('App\Project');

     }


}
