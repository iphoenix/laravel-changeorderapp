<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timesheet extends Model
{
    //use SoftDeletes;
    public $table = 'timesheet';

    //



    public $fillable = [
        'user_id','project_id','work_date','start_time','end_time','break','overtime','worker','hours',

    ];

    public $hidden = [
        'id',
    ];

    public $casts = [
        'worker' =>'array',
        'hours'=>'array',

    ];

    public $timestamps = true;

    public function user(){
        return  $this->belongsTo('App\User','user_id','id');

    }

    public function project(){
       return $this->belongsTo('App\Project','project_id','id');

    }

    public function worker(){
        return $this->belongsTo('App\Worker');

     }



}
