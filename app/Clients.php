<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    public $table = 'clients';
    //



    public $fillable = [
        'client_name',
    ];

    public $hidden = [
        'id',
    ];


}
