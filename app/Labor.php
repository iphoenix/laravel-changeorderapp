<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Labor extends Model
{
    public $table = 'labor';
    //



    public $fillable = [
        'name',
    ];

    public $hidden = [
        'id',
    ];

    public $timestamps = true;


}
