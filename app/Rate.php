<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    public $table = 'rate';
    //



    public $fillable = [
        'name',
    ];

    public $hidden = [
        'rate_id',
    ];


}
