<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $table = 'project';

    public $fillable = [
        'project_name','client_id','project_num','project_startdate','user_id','crew_id','crew_name','address','original_filename','filename','ticket_option','manager_id','company_id',
    ];

    protected $hidden = [
        'id',
    ];

    protected $casts = [
        'crew_name' => 'array',
        'crew_id'=>'array',
        'original_filename'=> 'array',
        'filename' => 'array',
        'project_num' => 'string',

    ];

    public function getClient(){
        return $this->belongsTo('App\Clients','client_id','id');
    }

    public function getManager(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function company(){
        return $this->belongsTo('App\Company');

     }

  


}
