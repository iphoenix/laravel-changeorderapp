<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'orders';
    //



    public $fillable = [
       'order_num', 'client_co_num','order_subject','project_id','order_date','efp_originalname','efp_filename','scope','exclusion','order_type','ticketid_collection','user_id',
    ];

    public $timestamps = true;

    public $casts = [
        'efp_originalname' =>'array',
        'efp_filename'=>'array',
        'ticketid_collection'=>'array',

    ];




    public function get_project(){
        return $this->belongsTo('App\Project','project_id','id');
     }

    public function get_user(){
    return $this->belongsTo('App\User','user_id','id');
    }




}
