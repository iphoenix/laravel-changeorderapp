<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\User;
use App\Project;
use App\Rate;
use App\labor;
use App\Worker;
use App\Timesheet;
use Auth;
use Session;
use Carbon;
use Storage;
use DB;

class TimesheetController extends Controller

{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function create(){
        
        $projects = Project::orderby('project_name','asc')->get();
        $workers = Worker::orderby('worker_name','asc')->get();

        return view('timesheet.new',['projects'=>$projects, 'workers'=>$workers, 'active'=>4]);
    }

    public function store(Request $request){

        $user = Auth::user();
        $new_timesheet = $request->all();
        unset($new_timesheet['_token']);
        $new = Timesheet::create($new_timesheet);
        return redirect()->back()->withSuccess("The new timesheet has been created!");
    }

    public function update(Request $request){

        $user = Auth::user();
        $update_timesheet = $request->all();
        unset($update_timesheet['_token']);
        unset($update_timesheet['timesheet_id']);
        $new = Timesheet::where('id',$request->timesheet_id)->update($update_timesheet);
        return redirect()->back()->withSuccess("The new timesheet has been created!");
    }

    public function index(){
        $timesheets = Timesheet::get();
       
        // $orders = Order::get();

        return view('timesheet.list',['timesheets'=>$timesheets, 'active'=>4]);

    }

    public function edit($id){
        $projects = Project::orderby('project_name','asc')->get();
        $workers = Worker::orderby('worker_name','asc')->get();
        $timesheet = Timesheet::find($id);
        return view('timesheet.edit',['projects'=>$projects,'timesheet'=>$timesheet,'workers'=>$workers,'active'=>4]);
    }

    public function destroy(Request $request){
        Timesheet::destroy($request->record_id);
        return response()->json(array(['success'=>true]));

    }

    public function show(){

        //$labors = Labor::orderby('name','asc')->get();
        $workers = Worker::orderby('worker_name','asc')->get();
        return view('timesheet.setting',['workers'=>$workers,'active'=>4]);

    }

    public function worker_create(Request $request){
        $name = $request->name;
        $com = Worker::where('worker_name',$name)->count();

        if($com < 1 ){
            $newClient = Worker::create(['worker_name'=>$name]);
            return response()->json(array('success'=>'true'));
        } else {
            return response()->json(array('success'=>'false'));
        }

    }

    public function worker_delete(Request $request){
        Worker::destroy($request->record_id);
        return response()->json(array('success'=>true));
    }

    public function worker_update(Request $request){
        Worker::where('id',$request->worker_id)->update([
            'worker_name'=>$request->name,
        ]);
        return response()->json(array('success'=>true));
    }


}
