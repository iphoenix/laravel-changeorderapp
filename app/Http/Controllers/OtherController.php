<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Rate;
use App\Labor;
use Auth;
use Session;
use Carbon;
use Storage;
use DB;

class OtherController extends Controller

{

    public function __construct()
    {
      $this->middleware('auth');
    }

    public function ticket_setting(){

        $labors = Labor::orderby('name','asc')->get();
        $rates = Rate::orderby('name','asc')->get();
        return view('ticket.setting',['rates'=>$rates,'labors'=>$labors,'active'=>2]);

    }

    public function rate_create(Request $request){
        $name = $request->name;
        $com = Rate::where('name',$name)->count();

        if($com < 1 ){
            $newClient = Rate::create(['name'=>$name]);
            return response()->json(array('success'=>'true'));
        } else {
            return response()->json(array('success'=>'false'));
        }

    }

    public function rate_delete (Request $request){
        Rate::where('rate_id',$request->record_id)->delete();
        return response()->json(array('success'=>true));
    }

    public function rate_update(Request $request){
        Rate::where('rate_id',$request->rate_id)->update([
            'name'=>$request->name,
        ]);
        return response()->json(array('success'=>true));
    }

    public function labor_create(Request $request){
        $name = $request->new_client_name;
        $com = Labor::where('name',$name)->first();

        if($com === null ){
            $newClient = Labor::create(['name'=>$name]);
            return response()->json(array('success'=>'true'));
        } else {
            return response()->json(array('success'=>'false'));
        }
    }

    public function labor_delete(Request $request){
        Labor::where('labor_id',$request->record_id)->delete();
        return response()->json(array('success'=>true));
    }

    public function labor_update(Request $request){
        Labor::where('labor_id',$request->labor_id)->update([
            'name'=>$request->name,
        ]);
        return response()->json(array('success'=>true));
    }

}
