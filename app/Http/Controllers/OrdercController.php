<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\User;
use App\Clients;
use App\Project;
use App\Constant;
use App\Rate;
use App\labor;
use App\Ticket;
use App\Order;
use App\Orderhistory;
use App\Setting;

use Auth;
use Storage;

class OrdercController extends Controller

{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function create(){
        $projects = Project::orderby('project_name','asc')->get();
        $tickets = Ticket::orderby('ticket_num','asc')->get();

        return view('order.new',['projects'=>$projects, 'tickets'=>$tickets,'active'=>3]);
    }

    public function edit($id){

        $projects = Project::orderby('project_name','asc')->get();
        $tickets = Ticket::orderby('ticket_num','asc')->get();
        $order = Order::find($id);
        $order_history = Orderhistory::where('order_id',$id)->orderby('created_at','asc')->get();
        $latest_activity = Orderhistory::where('order_id',$id)->orderby('created_at','desc')->first();
        $mycom = Constant::where('type','my_com_name')->first();

        return view('order.edit',['projects'=>$projects, 'tickets'=>$tickets,'order'=>$order,'active'=>3]);

    }

    public function order_update(Request $request){
        $user = Auth::user();
        

        $validator = Validator::make($request->all(), [
            'photos' => 'required',
          
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withError("Please select Image files!");
        }



        $filename_array = array();
        $original_filename = array();
        $extension_array = array();
        foreach($request->file('photos') as $one){
            if($one->extension() === null){
                return redirect()->back()->withErrors("Please upload usable file!");
            }
            array_push($extension_array, $one->extension());
        }

        if(!in_array('bin',$extension_array) ){
            $existed_files = Order::find($request->order_id);
            foreach($existed_files->efp_originalname as $file){
                if(Storage::exists('/public/rfp/'.$file)){
                    unlink(storage_path('app/public/rfp/'.$file));
                }
            }


            foreach($request->file('photos') as $one){
                $fileName=$one->getClientOriginalName();
                //$extention = $one->extension();
                $path = Storage::putFile('public/rfp',$one);
                $filter = explode('/',$path);
                array_push($filename_array,$filter[2]);
                array_push($original_filename,$fileName);
            }
            //var_dump($filename_array);
            $new_order = $request->all();
            //var_dump($new_order);
            $new = Order::where('id',$request->order_id)->update([
                'order_num' =>$new_order['order_num'],
                'client_co_num' =>$new_order['client_co_num'],
                'order_subject' =>$new_order['order_subject'],
                'project_id' =>$new_order['project_id'],
                'order_date' =>$new_order['order_date'],
                'scope' =>$new_order['scope'],
                'exclusion' =>$new_order['exclusion'],
                'user_id' =>$user->id,
                'order_type'=>$new_order['order_type'],
                'ticketid_collection'=>$new_order['ticketid_collection'],
                'efp_originalname'=>$filename_array,
                'efp_filename'=>$original_filename,
            ]);

            $new_history = new Orderhistory();
            $new_history->user_id = Auth::user()->id;
            $new_history->order_id = $request->order_id;
            $new_history->history_status = '4';
            $new_history->save();

            return redirect()->back()->withSuccess("The Order has been updated!");

        } else {
            return redirect()->back()->withErrors("Please upload .xls or .csv");
        };
    }

    public function print($id){
        $order = Order::find($id);
        $setting = Setting::first();
        $order['subtotal'] = $order->balance;
        $order['payments'] = $order->balance;
        $order['total'] =$order->balance;
        $to_company = Constant::where('type','my_com_name')->first();
        $submit_date = Orderhistory::where('history_status','0')->orderby('created_at','asc')->first();


        $pdf = \PDF::loadView('order.print',['order'=>$order,'setting'=>$setting,'to_company'=>$to_company,'submit_date'=>$submit_date])->setPaper('a4', 'landscape');
        return $pdf->download('order_'.$order->order_subject.'.pdf');
        return redirect()->back()->withSuccess('The PDF file has been generated!');
    }

    public function tickets($id){
        $tickets = Ticket::where('project_id',$id)->select('id','ticket_num','ticket_subject')->get();
        if(!empty($tickets)){
            return response()->json(array('success'=>'true','tickets'=>$tickets));
        } else {
            return response()->json(array('success'=>'false',));
        }

    }


    public function store(Request $request){
        
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'photos' => 'required',
          
        ]);

        if ($validator->fails()) {
            return back()->withError("Please select files!");
        }

        $filename_array = array();
        $original_filename = array();
        $extension_array = array();
        foreach($request->file('photos') as $one){
            if($one->extension() === null){
                return back()->withErrors("Please upload usable file!");
            }
            array_push($extension_array, $one->extension());
        }

        if(!in_array('bin',$extension_array) ){
            foreach($request->file('photos') as $one){
                $fileName=$one->getClientOriginalName();              
                $path = Storage::putFile('public/rfp',$one);
                $filter = explode('/',$path);
                array_push($filename_array,$filter[2]);
                array_push($original_filename,$fileName);
            }
          
            $new_order = $request->all(); 
            $new = Order::create([
                'order_num' =>$new_order['order_num'],
                'client_co_num' =>$new_order['client_co_num'],
                'order_subject' =>$new_order['order_subject'],
                'project_id' =>$new_order['project_id'],
                'order_date' =>$new_order['order_date'],
                'scope' =>$new_order['scope'],
                'exclusion' =>$new_order['exclusion'],
                'user_id' =>$user->id,
                'order_type'=>$new_order['order_type'],
                'ticketid_collection'=>$new_order['ticketid_collection'],
                'efp_originalname'=>$filename_array,
                'efp_filename'=>$original_filename,
            ]);

            $new_history = new Orderhistory();
            $new_history->user_id = Auth::user()->id;
            $new_history->order_id = $new->id;
            $new_history->history_status = '0';
            $new_history->save();



             return back()->withSuccess("The new order has been created!");

        } else {
            echo '432423';
            //return back()->withErrors("Please upload .xls or .csv");
        };
    }


    public function index(){

        $orders_draft = Order::where('status',0)->get();
        $orders = Order::get();
        $total_open = Order::where('status','0')->sum('balance');
        $total_paid = Order::where('complete',1)->sum('balance');

        return view('order.list',[ 'orders'=>$orders,'active'=>3,'orders_draft'=>$orders_draft,'total_open'=>$total_open,'total_paid'=>$total_paid]);

    }

    public function order_view($id){

        $order = Order::find($id);
        $order_history = Orderhistory::where('order_id',$id)->orderby('created_at','asc')->get();
        $latest_activity = Orderhistory::where('order_id',$id)->orderby('created_at','desc')->first();
        $mycom = Constant::where('type','my_com_name')->first();
        $tickets = $order->ticketid_collection;
        $ticket_array = array();
        foreach($tickets as $ticket){
            $ticket_one = Ticket::find($ticket);
            array_push($ticket_array, $ticket_one->ticket_subject);
        }

        return view('order.view',['order'=>$order, 'order_history'=>$order_history,  'latest_activity'=>$latest_activity,'mycom'=>$mycom->constant, 'ticket_array'=>$ticket_array,'active'=>3]);

    }

    public function order_delete($id){
        $order = Order::where('id',$id)->first();

        foreach($order->efp_originalname as $file){
            if(Storage::exists('/public/rfp/'.$file)){
                unlink(storage_path('app/public/rfp/'.$file));
            }
        }



        Order::destroy($id);
        return redirect('/order')->withSuccess("The  order has been deleted!");

    }

    public function order_submit($id){
        Order::where('id',$id)->update(['status'=>0]);
        $new_history = new Orderhistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->order_id = $id;
        $new_history->history_status = '0';
        $new_history->save();
        return redirect('/order/'.$id.'/view')->withSuccess("A selected ticket has been submitted!");
    }

    public function payment_update(Request $request){
        $order = Order::where('id',$request->id)->update(['balance'=>$request->balance]);
        if($order){
            return response()->json(array('success'=>'true','balance'=>$request->balance,));
        } else {
            return response()->json(array('success'=>'true','balance'=>$request->balance,));
        }

    }

    public function order_revise($id){
        Order::where('id',$id)->update(['status'=>1]);
        $new_history = new Orderhistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->order_id = $id;
        $new_history->history_status = '1';
        $new_history->save();
        return redirect('/order/'.$id.'/view')->withSuccess("A selected ticket has been revised!");
    }

    public function order_approve($id){
        Order::where('id',$id)->update(['status'=>2]);
        $new_history = new Orderhistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->order_id = $id;
        $new_history->history_status = '2';
        $new_history->save();
        return redirect('/order/'.$id.'/view')->withSuccess("A selected ticket has been approved!");
    }



    public function order_close($id){
        Order::where('id',$id)->update(['status'=>3]);
        $new_history = new Orderhistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->order_id = $id;
        $new_history->history_status = '3';
        $new_history->save();
        return redirect('/order/'.$id.'/view')->withSuccess("A selected ticket has been closed!");
    }

    public function order_oapprove($id){
        $order = Order::find($id);
        if($order->approved == 1){
            Order::where('id',$id)->update(['approved'=>0]);
        } else {
            Order::where('id',$id)->update(['approved'=>1]);
        }

        $new_history = new Orderhistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->order_id = $id;
        $new_history->history_status = '4';
        $new_history->save();
        return redirect('/order/'.$id.'/view')->withSuccess("A selected ticket has been approved!");
    }
    public function order_complete($id){

        $order = Order::find($id);
        if($order->complete == 1){
            Order::where('id',$id)->update(['complete'=>0]);
        } else {
            Order::where('id',$id)->update(['complete'=>1]);
        }

        $new_history = new Orderhistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->order_id = $id;
        $new_history->history_status = '4';
        $new_history->save();
        return redirect('/order/'.$id.'/view')->withSuccess("A selected ticket has been approved!");
    }

    public function history_clear($id){
        $first_history = Orderhistory::where('order_id',$id)->orderby('created_at','asc')->first();
        $order = orderhistory::where('order_id',$id)->where('id','!=',$first_history->id)->delete();

        return redirect('/order/'.$id.'/view')->withSuccess("The order history has been cleared!");
    }


}
