<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\User;
use App\Clients;
use App\Project;
use App\Rate;
use App\labor;
use App\Ticket;
use App\Order;
use App\Tickethistory;
use Auth;
use App\Setting;
use Session;
use Carbon;
use Storage;
use DB;

class OrderController extends Controller

{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index(){
        $projects = Project::get();
        $tickets = Ticket::get();

        return view('order.list',['projects'=>$projects, 'tickets'=>$tickets]);

    }

    public function create (){
        $projects = Project::get();
        $tickets = Ticket::get();

        return view('order.new',['projects'=>$projects, 'tickets'=>$tickets,'active'=>3]);
    }

}
