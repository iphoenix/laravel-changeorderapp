<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Exception;
use App\User;
use App\Clients;
use App\Project;
use App\Rate;
use App\Labor;
use App\Ticket;
use App\Timesheet;
use App\Tickethistory;
use App\Company;
use App\Constant;
use App\Order;
use Auth;
use App\Setting;
use App\Projecthistory;
use Storage;
use DB;

class ProjectController extends Controller

{
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function index(){

        return view('/index');
    }

    public function createProjectView(){
        $users = User::orderby('name','asc')->get();
        $clients = Clients::orderby('client_name','asc')->get();
        $companies = Company::orderby('name','asc')->get();

        return view('projects.create_project',['users'=>$users,'clients'=>$clients,'companies'=>$companies,'active'=>1]);
    }


    public function create_client(Request $request){
        $client_name = $request->new_client_name;
        $client = Clients::where('client_name',$client_name)->first();
        if($client === null ){
            $newClient = new Clients;
            $newClient->client_name = $client_name;
            $newClient->save();
            return response()->json(array('success'=>'true','id'=>$newClient->id,'name'=>$newClient->client_name));
        } else {
            return response()->json(array('success'=>'false'));
        }
    }

    public function company_create(Request $request){

        $name = $request->name;
        $com = Company::where('name',$name)->first();

        if($com === null ){
            $new = $request->all();
            $new['user_id'] = Auth::user()->id;
            $newClient = Company::create($new);
            return response()->json(array('success'=>'true','id'=>$newClient->id,'name'=>$newClient->client_name));
        } else {
            return response()->json(array('success'=>'false'));
        }

    }

    public function company_update(Request $request){
        $data = $request->all();
        unset($data['com_id']);
        unset($data['_token']);
        if(Company::where('id',$request->com_id)->update($data)){
            return response()->json(array('success'=>true));
        } else {
            return response()->json(array('success'=>false));
        }

    }

    public function company_delete(Request $request){
        if(Company::destroy($request->record_id)){
            return response()->json(array('success'=>true));
        } else {
            return response()->json(array('success'=>false));
        }
    }


    public function client_update(Request $request){
        if(Clients::where('id',$request->client_id)->update([
            'client_name'=>$request->name,
        ])){
            return response()->json(array('success'=>true));
        } else {
            return response()->json(array('success'=>false));
        }
    }




    public function project_list(){

        $project_list = Project::orderby('created_at','asc')->get();
        $users = User::all();
        $clients = Clients::all();
        return view('projects.project_list',['project_list'=>$project_list,'users'=>$users,'clients'=>$clients,'active'=>1]);
    }

    public function project_file($id){
        $file = Project::find($id);
        $exists = Storage::exists('public/workspace/'.$file->filename);
        if($exists){
            //$name  ='sdfsdfds.jpg';
            //Storage::download("app/public/workspace/".$file->filename, $name);
            return response()->download(storage_path("app/public/workspace/".$file->filename));
        } else {
            return redirect()->back()->withError('No existed file!');
        }
    }

    public function project_hisory(){
        Projecthistory::query()->delete();

        return redirect()->back()->withSuccess("The project history has been cleared!");
    }

    public function ticket_history_clear($id){

        $first_history = TicketHistory::where('ticket_id',$id)->orderby('created_at','asc')->first();
        $order = TicketHistory::where('ticket_id',$id)->where('id','!=',$first_history->id)->delete();

        return redirect()->back()->withSuccess("The Ticket history has been cleared!");

    }

    public function project_edit(){
        $id = 125;
        $project = Project::find($id);
        $users = User::orderby('name','asc')->get();
        $clients = Clients::orderby('client_name','asc')->get();
        $project_log = Projecthistory::where('project_id',$id)->get();
        $companies = Company::orderby('name','asc')->get();

        return view('projects.edit',['users'=>$users,'clients'=>$clients,'active'=>1,'project'=>$project,'project_log'=>$project_log,'companies'=>$companies]);
    }

    public function project_view(){
        $id = 125;
        $project = Project::find($id);
        $users = User::orderby('name','asc')->get();
        $clients = Clients::orderby('client_name','asc')->get();
        $project_log = Projecthistory::where('project_id',$id)->get();
        $companies = Company::orderby('name','asc')->get();

        return view('projects.view',['users'=>$users,'clients'=>$clients,'active'=>1,'project'=>$project,'project_log'=>$project_log,'companies'=>$companies]);
    }

    public function project_setting(){

        $clients = Clients::orderby('client_name','asc')->get();
        $companies = Company::orderby('name','asc')->get();
        return view('projects.setting',['clients'=>$clients,'companies'=>$companies,'active'=>1]);

    }

    public function client_delete(Request $request){
        Clients::find($request->record_id)->delete();
        return response()->json(array('success'=>true));
    }

    public function delete_record(Request $request){
        Project::find($request->record_id)->delete();
        Ticket::where('project_id',$request->record_id)->delete();
        Timesheet::where('project_id',$request->record_id)->delete();
        Order::where('project_id',$request->record_id)->delete();

        return response()->json(array('success'=>true));
    }

    public function change_p_status(Request $request){
        $status = 0;
        if(Project::find($request->record_id)->p_status == 1){
            Project::where('id',$request->record_id)->update(['p_status'=> 0]);
            $status = 0;
        } else {
            Project::where('id',$request->record_id)->update(['p_status'=> 1]);
            $status = 1;
        }

        return response()->json(array('success'=>true,'status'=>$status));

    }

    public function project_update(Request $request){
        $validation = $request->validate([
            'fileupload' => 'required',
            'user_id'=>'required',
         
        ]);
        $user = Auth::user();
    

        $filename_array = array();
        $original_filename = array();
        $extension_array = array();
        foreach($request->file('fileupload') as $one){
            if($one->extension() === null){
                return redirect()->back()->withErrors("Please upload usable file!");
            }
            array_push($extension_array, $one->extension());
        }

        if(!in_array('bin',$extension_array) ){
            foreach($request->file('fileupload') as $one){

                $fileName=$one->getClientOriginalName();
                //echo $fileName;
                //$extention = $one->extension();
                $path = Storage::putFile('public/project',$one);
                $filter = explode('/',$path);
                array_push($filename_array,$filter[2]);
                array_push($original_filename,$fileName);

            }
            // End : To upload multiple files
            $existed_files = Project::find($request->project_id);
            //var_dump($existed_files);
            foreach($existed_files->filename as $file){
                if(Storage::exists('/public/project/'.$file)){
                    unlink(storage_path('app/public/project/'.$file));
                }
            }
            $request_values = $request->all();
            unset($request_values['project_id']);
            unset($request_values['_token']);
            unset($request_values['fileupload']);

            Project::where('id',$request->project_id)->update($request_values);

            // return;
            $new_project = Project::find($request->project_id);
            $new_project->original_filename = $original_filename;
            $new_project->filename = $filename_array;
            $new_project->save();

            Projecthistory::create([
                'user_id'=>$user->id,
                'project_id'=>$request->project_id,
                'status'=>1,
            ]);


            return redirect()->back()->withSuccess("The Project has been updated!");

        } else {
            return redirect()->back()->withErrors("Please upload .xls or .csv");
        };

    }

    public function createProject(Request $request){

        $validation = $request->validate([
            'fileupload' => 'required',
            'user_id'=>'required',
            // 'fileupload.*' => 'mimes:jpg,png,jpeg,gif,svg:max:2048',
        ]);
        $user = Auth::user();

        // start : To upload multiple files

        $filename_array = array();
        $original_filename = array();
        $extension_array = array();
        foreach($request->file('fileupload') as $one){
            if($one->extension() === null){
                return redirect()->back()->withErrors("Please upload usable file!");
            }
            array_push($extension_array, $one->extension());
        }

        if(!in_array('bin',$extension_array) ){
            foreach($request->file('fileupload') as $one){

                $fileName=$one->getClientOriginalName();
                echo $fileName;
                //$extention = $one->extension();
                $path = Storage::putFile('public/project',$one);
                $filter = explode('/',$path);
                array_push($filename_array,$filter[2]);
                array_push($original_filename,$fileName);

            }
            // End : To upload multiple files
            $request_values = $request->all();
            $new_project = Project::create($request_values);
            $new_project->original_filename = $original_filename;
            $new_project->filename = $filename_array;
            $new_project->save();
            Projecthistory::create([
                'user_id'=>$user->id,
                'project_id'=>$new_project->id,
                'status'=>0,
            ]);
            return redirect()->back()->withSuccess("The Project has been created!");

        } else {
            return redirect()->back()->withErrors("Please upload .xls or .csv");
        };

    }

    public function new_ticketView(){

        $users = User::orderby('name','asc')->get();
        $projects = Project::orderby('project_name','asc')->get();
        $rates = Rate::all();
        $labors = Labor::all();

        return view('ticket.new_ticket',['users'=>$users,'projects'=>$projects,'active'=>2,'labors'=>$labors,'rates'=>$rates]);

    }

    public function ticket_print($id){

        $ticket = Ticket::find($id);
        $setting = Setting::first();
        $to_company = Constant::where('type','my_com_name')->first();
        $submit_date = Tickethistory::where('history_status','0')->orderby('created_at','asc')->first();
        $pdf = \PDF::loadView('ticket.print',['ticket'=>$ticket,'setting'=>$setting,'to_company'=>$to_company,'submit_date'=>$submit_date]);
        return $pdf->download('ticket'.$ticket->ticket_subject.'.pdf');
        return redirect()->back()->withSuccess('The PDF file has been generated!');

    }

    public function new_ticketCreate(Request $request){
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'photos' => 'required',
            'photos.*' => 'mimes:jpg,png,jpeg,gif,svg:max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withError("Please select Image files!");
        }


        $exists = false;
        $filename_array = array();
        $original_filename = array();
        if($exists){
            return redirect()->back()->withError("The file with same name already exsit!");
        } else {
            foreach($request->file('photos') as $one){
                $fileName=$one->getClientOriginalName();
                //$extention = $one->extension();
                $path = Storage::putFile('public/photos',$one);
                $filter = explode('/',$path);
                array_push($filename_array,$filter[2]);
                array_push($original_filename,$fileName);
            }
            //var_dump($filename_array);
            $ticket = $request->all();

            $labor = array([
                'labor_type' => $request->labor_type,
                'labor_rate_type' => $request->labor_rate_type,
                'labor_headcount' => $request->labor_headcount,
                'labor_hours' => $request->labor_hours,
                'labor_rate' => $request->labor_rate,

            ]);



            $labor_des = array([
                'description' => $request->labor_description,
                'amount' => $request->labor_amount,
            ]);



            $material = array([
                'type' => $request->material_type,
                'quentity' => $request->material_quentity,
                'unit' => $request->material_unit,
                'hours' => $request->material_hours,
            ]);

            $material_des = array([
                'description' => $request->material_description,
                'amount' => $request->material_amount,
            ]);

            $equipment = array([
                'type' => $request->equip_type,
                'quentity' => $request->equip_quantity,
                'unit' => $request->equip_unit,
                'hours' => $request->equip_hours,
            ]);
            $equipment_des = array([
                'description' => $request->equip_description,
                'amount' => $request->equip_amount,
            ]);
            $new_ticket_id = DB::table('ticket')->insertGetId(
                [
                    'project_id' =>$ticket['project_id'],
                    'ticket_subject' =>$ticket['ticket_subject'],
                    'ticket_num' =>$ticket['ticket_num'],
                
                    'start_date' =>$ticket['start_date'],
                    'end_date' =>$ticket['start_date'],
                    'work_description' =>$ticket['work_description'],
                    'labor_collection' =>json_encode($labor),
                    'material_collection' =>json_encode($material),
                    'equipment_collection' =>json_encode($equipment),
                    'labor_des' =>json_encode($labor_des),
                    'material_des' =>json_encode($material_des),
                    'equipment_des' =>json_encode($equipment_des),
                    'photoname' => json_encode($filename_array),
                    'original_photoname' => json_encode($original_filename),
                    'user_id' =>$user->id,

                ]
            );
            //var_dump($new_ticket_id);
            $new_history = new Tickethistory();
            $new_history->user_id = $user->id;
            $new_history->ticket_id = $new_ticket_id;
            $new_history->history_status = '0';
            $new_history->save();
            // var_dump($request->all());
            // return;

            return redirect()->back()->withSuccess("The new ticket has been created!");
        }
    }
    public function ticket_update(Request $request){
        $user = Auth::user();
        $request->validate([
            'photos' => 'required',
            'photos.*' => 'mimes:jpg,png,jpeg,gif,svg:max:2048',
        ]);

        $filename_array = array();
        $original_filename = array();
        // Start : store immage to storage
        foreach($request->file('photos') as $one){
            $fileName=$one->getClientOriginalName();
            //$extention = $one->extension();
            $path = Storage::putFile('public/photos',$one);
            $filter = explode('/',$path);
            array_push($filename_array,$filter[2]);
            array_push($original_filename,$fileName);
        }
        // end : store immage to storage
        $ticket = $request->all();
        // var_dump($ticket);
        // return;

        $labor = array([
            'labor_type' => $request->labor_type,
            'labor_rate_type' => $request->labor_rate_type,
            'labor_headcount' => $request->labor_headcount,
            'labor_hours' => $request->labor_hours,
            'labor_rate' => $request->labor_rate,

        ]);



        $labor_des = array([
            'description' => $request->labor_description,
            'amount' => $request->labor_amount,
        ]);



        $material = array([
            'type' => $request->material_type,
            'quentity' => $request->material_quentity,
            'unit' => $request->material_unit,
            'hours' => $request->material_hours,
        ]);

        $material_des = array([
            'description' => $request->material_description,
            'amount' => $request->material_amount,
        ]);

        $equipment = array([
            'type' => $request->equip_type,
            'quentity' => $request->equip_quantity,
            'unit' => $request->equip_unit,
            'hours' => $request->equip_hours,
        ]);
        $equipment_des = array([
            'description' => $request->equip_description,
            'amount' => $request->equip_amount,
        ]);

        $existed_files = ticket::find($request->ticket_id);
        //var_dump($existed_files);
        foreach(json_decode($existed_files->photoname) as $file){
            if(Storage::exists('/public/photos/'.$file)){
                unlink(storage_path('app/public/photos/'.$file));
            }
        }

        $update_ticket = Ticket::whereId($request->ticket_id)->update(
            [
                'project_id' =>$ticket['project_id'],
                'ticket_subject' =>$ticket['ticket_subject'],
                'ticket_num' =>$ticket['ticket_num'],
     
                'start_date' =>$ticket['start_date'],
                'end_date' =>$ticket['start_date'],
                'work_description' =>$ticket['work_description'],
                'labor_collection' =>json_encode($labor),
                'material_collection' =>json_encode($material),
                'equipment_collection' =>json_encode($equipment),
                'labor_des' =>json_encode($labor_des),
                'material_des' =>json_encode($material_des),
                'equipment_des' =>json_encode($equipment_des),
                'photoname' => json_encode($filename_array),
                'original_photoname' => json_encode($original_filename),
                'user_id' =>$user->id,

            ]
        );
        //var_dump($new_ticket_id);
        $new_history = new Tickethistory();
        $new_history->user_id = $user->id;
        $new_history->ticket_id = $request->ticket_id;
        $new_history->history_status = '5';
        $new_history->save();
        return redirect('ticketview/'.$request->ticket_id)->withSuccess("The new ticket has been created!");

    }

    public function ticket_delete($id){
       
       $ticket = Ticket::where('id',$id)->first();
       Tickethistory::where('ticket_id',$id)->delete();

       $order =  Order::where('ticketid_collection','like','%"'.$id.'"%')->delete();
       foreach(json_decode($ticket->photoname) as $file){
            if(Storage::exists('/public/photos/'.$file)){
                unlink(storage_path('app/public/photos/'.$file));
            }
        }
        Ticket::destroy($id);
    


        return redirect('ticket/list')->withSuccess("A selected ticket has been deleted!");
    }

    public function ticket_submit($id){
        Ticket::where('id',$id)->update(['t_status'=>1]);
        $new_history = new Tickethistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->ticket_id = $id;
        $new_history->history_status = '1';
        $new_history->save();
        return redirect('ticketview/'.$id)->withSuccess("A selected ticket has been submitted!");
    }

    public function ticket_revise($id){
        Ticket::where('id',$id)->update(['t_status'=>2]);
        $new_history = new Tickethistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->ticket_id = $id;
        $new_history->history_status = '2';
        $new_history->save();
        return redirect('ticketview/'.$id)->withSuccess("A selected ticket has been revised!");
    }

    public function ticket_approve($id){
        Ticket::where('id',$id)->update(['t_status'=>3]);
        $new_history = new Tickethistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->ticket_id = $id;
        $new_history->history_status = '3';
        $new_history->save();
        return redirect('ticketview/'.$id)->withSuccess("A selected ticket has been approved!");
    }

    public function ticket_close($id){
        Ticket::where('id',$id)->update(['t_status'=>4]);
        $new_history = new Tickethistory();
        $new_history->user_id = Auth::user()->id;
        $new_history->ticket_id = $id;
        $new_history->history_status = '4';
        $new_history->save();
        return redirect('ticketview/'.$id)->withSuccess("A selected ticket has been closed!");
    }

    public function ticket_edit($id){

        $users = User::all();
        $projects = Project::all();
        $rates = Rate::all();
        $labors = Labor::all();
        $ticket = DB::table('ticket')->join('project','project.id','ticket.project_id')->join('users','users.id','ticket.user_id')->select('ticket.*','project.ticket_option')->where('ticket.id', $id)->first();
        //var_dump($ticket);

        // $ticket = DB::table('ticket')->where('id', $id)->first();
        $labor = json_decode($ticket->labor_des, true);
        $material = json_decode($ticket->material_des,true);
        $equipment = json_decode($ticket->equipment_des,true);
        $labor_collection = json_decode($ticket->labor_collection,true);
        $material_collection = json_decode($ticket->material_collection, true);
        $equipment_collection = json_decode($ticket->equipment_collection,true);
        //var_dump($material_collection[0]);
        //var_dump($ticket);

        return view('ticket.edit',['users'=>$users,'projects'=>$projects,'active'=>2,'labors'=>$labors,'rates'=>$rates,'ticket'=>$ticket,'labor_des'=>$labor[0],'material_des'=>$material[0],'equipment_des'=>$equipment[0],'material_collection'=>$material_collection[0], 'labor_collection'=>$labor_collection[0],'equipment_collection'=>$equipment_collection[0]]);
    }

    public function ticket_list(){

        $tickets = Ticket::orderby('ticket_subject','asc')->get();
        $active_project = Project::where('p_status','1')->orderBy('id','asc')->get();
        $projects = Project::all();
        $date = array();
        foreach($tickets as $ticket){
            $year = date('Y',strtotime($ticket->created_at));

            if(!in_array($year,$date)){
                array_push($date,$year);
            }
        }
        // print_r($date);
        // return;
        return view('ticket.list',['tickets'=>$tickets, 'projects'=>$projects,'active_project'=>$active_project,'date'=>$date,'active'=>2]);
    }

    public function ticket_view($id){

        $ticket = DB::table('ticket')->join('project','project.id','ticket.project_id')->join('users','users.id','ticket.user_id')->select('ticket.*','project.project_name','project.project_num','users.name')->where('ticket.id', $id)->first();

        $ticket_history = DB::table('ticket_history')->join('users','users.id','ticket_history.user_id')->select('ticket_history.*','users.name')->where('ticket_history.ticket_id', $id)->get();
        $projects = Project::where('p_status','1')->get();
        $latest_activity = Tickethistory::where('ticket_id',$id)->orderby('created_at','desc')->first();
        $date = array();

        return view('ticket.view',['ticket'=>$ticket, ' projects'=>$projects, 'date'=>$date, 'ticket_history'=>$ticket_history,'latest_activity'=>$latest_activity,'active'=>2]);

    }

    public function setting(){
        $setting = Setting::first();
        $my_company = Constant::where('type','my_com_name')->first();
        return view('setting',['my_company'=>$my_company,'active'=>5]);
    }

    public function storesetting(Request $request){

        $data = $request->all();
        $setting = Setting::first();

        $setting->site_name = $request->site_name;
        $setting->site_email = $request->site_email;
        $setting->site_address1 = $request->site_address1;
        $setting->site_address2 = $request->site_address2;
        $setting->site_address3 = $request->site_address3;
        $setting->save();

        $my_company  = Constant::where('type','my_com_name')->first();
        $my_company->constant = $request->company;
        $my_company->save();

        return redirect('/setting')->withSuccess("The site has been updated!");



    }

    public function update_logo (Request $request){
        //var_dump($request->all());
        //return ;
        $data = $request->all();

        $request->validate([
            'photo' => 'required',
            'photo.*' => 'mimes:jpg,png,jpeg,gif,svg:max:2048',
        ]);

        $setting = Setting::first();

        $logo_url = $setting->logo_url;

        if(Storage::exists('public/photos/'.$logo_url)){
            unlink(storage_path('app//public/photos/'.$logo_url));
        }
        $path = Storage::putFile('public/photos',$request->photo);
        $filter = explode('/',$path);
        $setting->logo_url = $filter[2];
        $setting->save();

        return redirect('/setting')->withSuccess("The site has been updated!");



    }
}
